# Latent_space_GP_Selector_Minimum

**Author**: Fleytoux Yoann  
**Contact**: yoann.fleytoux@inria.fr  

## What is the purpose of this repository?
This repository contains a minimum implementation of Data-efficient learning of object-centric grasp preferences that you can access a preprint here: https://hal.univ-lorraine.fr/hal-03466780. 

We propose a data-efficient grasping pipeline (Latent Space GP Selector --- LGPS) that learns grasp preferences with only a few labels per object and generalizes to new views of this object.


In our pipeline, grasps candidates are encoded into image patches to be easily fed to convolutional neural networks.


These patches are used to train a variational convolutional autoencoder, so the patches can be encoded into a latent representation.
This allow us to train a GP classifier that will predict how well a grasp candidate match the preferences of a user.

***
## Installation
Tested with python=3.6, cuda 10.1, 

    conda update -n base -c defaults conda
    conda create -n LGPS_min python=3.6
    conda activate LGPS_min

    ~/anaconda3/envs/LGPS_min/bin/pip install opencv-python
    ~/anaconda3/envs/LGPS_min/bin/pip install Pillow
    ~/anaconda3/envs/LGPS_min/bin/pip install joblib
    ~/anaconda3/envs/LGPS_min/bin/pip install -U scikit-learn
    ~/anaconda3/envs/LGPS_min/bin/pip install matplotlib
    ~/anaconda3/envs/LGPS_min/bin/pip install GPy
    ~/anaconda3/envs/LGPS_min/bin/pip install -U scikit-image
    ~/anaconda3/envs/LGPS_min/bin/pip install tensorflow
    ~/anaconda3/envs/LGPS_min/bin/pip install tensorflow-addons
    ~/anaconda3/envs/LGPS_min/bin/pip install torch
    ~/anaconda3/envs/LGPS_min/bin/pip install gpytorch

or 

pip3 install -r requirements.txt

***
## Download latest trained models

##  Demo:
* For a quick demo you can go into scripts and launch:

  * **demo.py**

It will load the RGBD scene stored in ./data/test_scene/ and print the predicted scores

***

### File Structure
```angular2html
├── lgps
|   |── data
|   |   |── config_profiles # ini config files for differents experiments
|   |   |── trained_models # trained models for supervised and unsupervised models
|   |   |── test_scenes # demo test scene (cornell dataset and our dataset)
|   |── scripts 
|   |   |── common # utility functions
|   |   |── models
|   |   |  |── common # utility functions for both types of models
|   |   |  |── supervised_models # classifiers and regressors class models
|   |   |  |── unsupervised_models # vae class models
└── ...
```
***
