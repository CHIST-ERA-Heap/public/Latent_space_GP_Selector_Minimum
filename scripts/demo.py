import numpy as np
import cv2
import configparser
from common.setup_stuff_util import setup_feature_extraction_stuff, setup_object_classifier, setup_classifier, setup_length_regressor

def main(config, cv_image, depth_raw, grasps_candidates_list):

    representation_class = setup_feature_extraction_stuff(config)
    classifier_object = setup_object_classifier(config)
    classifier = setup_classifier(config, representation_class)
    # length_regressor = setup_length_regressor(config)

    current_scene_id = -1
    dict_rgb_scene = {}
    dict_depth_raw = {}
    dict_rgb_scene[current_scene_id] = cv_image
    dict_depth_raw[current_scene_id] = depth_raw

    if classifier.by_object:
        # sort grasps_candidates by object prediction
        grasps_candidates_list, _, _, elapsed_time = classifier_object.predict(
            grasps_candidates_list,
            dict_rgb_scene, dict_depth_raw)

    gp_scores, bvae_mu, bvae_var, elapsed_time = classifier.predict(grasps_candidates_list, dict_rgb_scene,
                                                                         dict_depth_raw)

    print('gp_scores: ', gp_scores)
    print('bvae_mu: ', bvae_mu.shape)
    print('bvae_var: ', bvae_var.shape)
    print('elapsed_time: ', elapsed_time)

if __name__ == '__main__':

    config_path = '../data/config_profiles/demo_config.ini'
    rgb_scene_path = '../data/test_scene/inria/security_glasses.png'
    depth_scene_path = '../data/test_scene/inria/security_glasses.npy'

    cv_image = cv2.imread(rgb_scene_path)
    depth_raw = np.load(depth_scene_path)
    config = configparser.ConfigParser()
    config.read(config_path)

    grasps_candidates_list =  [[-1, -1, -1, 846.0, 285.0, 0.5800000429153442, 0.0, 0.0, -0.5287074722984426, 43.82407762880566, 21.91203881440283, 1.0185130834579468]]

    main(config, cv_image, depth_raw, grasps_candidates_list)


