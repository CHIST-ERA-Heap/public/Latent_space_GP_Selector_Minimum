import numpy as np
import math
import cv2

#todo 2 definition
def get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                        dim_grasp_patch=96, dim_grasp_patch_y=None, verbose = False):
    #rows, cols, canal = depth_data.shape
    rows = depth_data.shape[0]
    cols = depth_data.shape[1]

    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    if verbose:
        print("dim_grasp_patch: ", dim_grasp_patch)
        print("DIM: ", DIM)
        print("DIM_y: ", DIM_y)
        print("depth_data.shape: ", depth_data.shape)

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst_d = cv2.warpAffine(depth_data, M, (cols, rows))
    dst_d = dst_d[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
            int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    # generate depth grayscale patch
    # cf autolab perception

    where_are_NaNs = np.isnan(dst_d)
    max_d = np.max(dst_d)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_are_NaNs] = max_d


    if verbose:
        print("dst_d.shape: ", dst_d.shape)
        print("mean dst_d: ", np.mean(dst_d))

    return dst_d  # raw depth

def get_depth_with_width(grasp_position_x, grasp_position_y, rotation_in_radian, cloud_points, gripper_length_pixel,
                         dim_grasp_patch_y=6, fix_delta=0.00, verbose=False):
    if verbose:
        print("grasp_position_x: ", grasp_position_x)
        print("grasp_position_y: ", grasp_position_y)
        print("rotation_in_radian: ", rotation_in_radian)
        print("gripper_length_pixel: ", gripper_length_pixel)
        print("cloud_points.shape: ", cloud_points.shape)
        print("max(cloud_points): ", np.max(cloud_points))

    if gripper_length_pixel <=5:
        gripper_length_pixel = 5
    points_on_line = get_raw_depth_patch(cloud_points, grasp_position_x, grasp_position_y,
                                        math.degrees(rotation_in_radian), dim_grasp_patch=gripper_length_pixel, dim_grasp_patch_y=dim_grasp_patch_y, verbose=verbose)

    if verbose:
        print("points_on_line: ", points_on_line)
        print("points_on_line.shape: ", points_on_line.shape)
        print("max(points_on_line): ", np.max(points_on_line))
    try:
        depth_min = np.min(points_on_line)
        depth_max = np.max(points_on_line)
    except Exception as e:
        print(e)
        depth_min = np.median(cloud_points)
        depth_max = depth_min

    return depth_min+fix_delta, depth_max+fix_delta


def from_pcd(pcd_filename, shape, default_filler=0, index=None):
    """
        Create a depth image from an unstructured PCD file.
        If index isn't specified, use euclidean distance, otherwise choose x/y/z=0/1/2
    """
    img = np.zeros(shape)
    if default_filler != 0:
        img += default_filler

    with open(pcd_filename) as f:
        for l in f.readlines():
            ls = l.split()

            if len(ls) != 5:
                # Not a point line in the file.
                continue
            try:
                # Not a number, carry on.
                float(ls[0])
            except ValueError:
                continue

            i = int(ls[4])
            r = i // shape[1]
            c = i % shape[1]

            if index is None:
                x = float(ls[0])
                y = float(ls[1])
                z = float(ls[2])

                img[r, c] = np.sqrt(x ** 2 + y ** 2 + z ** 2)

            else:
                img[r, c] = float(ls[index])

    return img / 1000.0

def inpaint( img, missing_value=0):
    """
    Inpaint missing values in depth image.
    :param missing_value: Value to fill in teh depth image.
    """
    # cv2 inpainting doesn't handle the border properly
    # https://stackoverflow.com/questions/25974033/inpainting-depth-map-still-a-black-image-border
    img = cv2.copyMakeBorder(img, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
    mask = (img == missing_value).astype(np.uint8)

    # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
    scale = np.abs(img).max()
    img = img.astype(np.float32) / scale  # Has to be float32, 64 not supported.
    img = cv2.inpaint(img, mask, 1, cv2.INPAINT_NS)

    # Back to original size and value range.
    img = img[1:-1, 1:-1]
    img = img * scale
    return img

def depth_txt_to_numpy(pcd_filename, shape):
    di = from_pcd(pcd_filename, shape)
    di = inpaint(di)
    di_array = np.asarray(di)
    return np.expand_dims(di_array, axis=2)  # check

def grasp_to_rectangle(center, angle, length, width, verbose = False):
    if verbose:
        print("from my_grasp_to_grconvnet_rectangle")
        print("center: ", center)
        print("angle: ", angle)
        print("length: ", length)
        print("width: ", width)

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))
    p3 = np.zeros((2, 1))
    p4 = np.zeros((2, 1))

    p1[0][0] = -int(length / 2)
    p1[1][0] = -int(width / 2)

    p2[0][0] = int(length / 2)
    p2[1][0] = -int(width / 2)

    p3[0][0] = int(length / 2)
    p3[1][0] = int(width / 2)

    p4[0][0] = -int(length / 2)
    p4[1][0] = int(width / 2)

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)
    p3_rotated = np.dot(rotation_matrix, p3)
    p4_rotated = np.dot(rotation_matrix, p4)

    p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
    p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
    p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
    p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

    # rectangle = np.array([p1_recentered,p2_recentered,p3_recentered,p4_recentered]).reshape((4,2)).astype(int)
    rectangle = np.array([p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
        (4, 2)).astype(int)
    return rectangle

def get_grasp_vector_with_rectangle(rectangle):
    # print("grasp :")
    # print(rectangle.shape)
    # print(rectangle)

    angle = get_angle(rectangle)
    width = get_width(rectangle)
    length = get_length(rectangle)
    center = get_center(rectangle)
    return float(center[1]), float(center[0]), float(angle), float(length), float(width)

def get_angle(points):
    """
    :return: Angle of the grasp to the horizontal.
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]
    #return (np.arctan2(-dy, dx) + np.pi/2) % np.pi - np.pi/2

    # def angle_trunc(a):get_angle
    #     while a < 0.0:
    #         a += math.pi * 2
    #     return a
    # return angle_trunc(math.atan2(dy, dx))
    # return (np.arctan2(-dy, dx) + np.pi / 2) % np.pi - np.pi / 2

    angle = math.atan2(dy, dx)
    while (angle > np.pi):
        angle -= 2 * np.pi
    while (angle < -np.pi):
        angle += 2 * np.pi
    if angle > np.pi / 2:
        angle = -np.pi / 2 + (angle - np.pi / 2)

    if angle < -np.pi / 2:
        angle = np.pi / 2 + (angle - -np.pi / 2)

    return angle



def get_center(points):
    """
    :return: Rectangle center point
    """
    return points.mean(axis=0).astype(np.int)

def get_length(points):
    """
    :return: Rectangle length (i.e. along the axis of the grasp)
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

def get_width(points):
    """
    :return: Rectangle width (i.e. perpendicular to the axis of the grasp)
    """
    dy = points[2, 1] - points[1, 1]
    dx = points[2, 0] - points[1, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

def load_rectangles_from_cornell(fname, verbose=False):
    grs = []
    with open(fname) as f:
        while True:
            # Load 4 lines at a time, corners of bounding box.
            p0 = f.readline()
            if not p0:
                break  # EOF
            p1, p2, p3 = f.readline(), f.readline(), f.readline()
            try:
                gr = np.array([
                    _gr_text_to_no(p0),
                    _gr_text_to_no(p1),
                    _gr_text_to_no(p2),
                    _gr_text_to_no(p3)
                ])
                if verbose:
                    print("load_rectangles_from_cornell: ", fname)
                    print("gr: ", gr)

                angle = get_angle(gr)
                width = get_width(gr)
                length = get_length(gr)
                center = get_center(gr)

                rotation_matrix = np.zeros((2, 2))
                rotation_matrix[0][0] = math.cos(angle)
                rotation_matrix[0][1] = -math.sin(angle)
                rotation_matrix[1][0] = math.sin(angle)
                rotation_matrix[1][1] = math.cos(angle)

                p1 = np.zeros((2, 1))
                p2 = np.zeros((2, 1))
                p3 = np.zeros((2, 1))
                p4 = np.zeros((2, 1))

                p1[0][0] = -int(length / 2)
                p1[1][0] = -int(width / 2)

                p2[0][0] = int(length / 2)
                p2[1][0] = -int(width / 2)

                p3[0][0] = int(length / 2)
                p3[1][0] = int(width / 2)

                p4[0][0] = -int(length / 2)
                p4[1][0] = int(width / 2)

                p1_rotated = np.dot(rotation_matrix, p1)
                p2_rotated = np.dot(rotation_matrix, p2)
                p3_rotated = np.dot(rotation_matrix, p3)
                p4_rotated = np.dot(rotation_matrix, p4)

                p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
                p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
                p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
                p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

                rectangle = np.array(
                    [p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
                    (4, 2)).astype(int)
                if verbose:
                    print("rectangle: ", rectangle)

                grs.append(rectangle)

            except ValueError:
                # Some files contain weird values.
                continue
    return grs


def _gr_text_to_no(l, offset=(0, 0)):
    """
    Transform a single point from a Cornell file line to a pair of ints.
    :param l: Line from Cornell grasp file (str)
    :param offset: Offset to apply to point positions
    :return: Point [y, x]
    """
    x, y = l.split()
    return [int(round(float(y))) - offset[0], int(round(float(x))) - offset[1]]


def find_center_of_mask(mask):
    centers = []

    print('mask.shape: ', mask.shape)
    print('mask min: ', np.min(mask))
    print('mask max ', np.max(mask))


    # Find contours:
    try:
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    except:
        # im, contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for i in range(len(contours)):
        try:
            moments = cv2.moments(contours[i])
            # centres.append((int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])))
            centers.append((int(moments['m01'] / moments['m00']), (int(moments['m10'] / moments['m00']))))
        except:
            pass
    if len(centers) == 0:
        centers = [(int(mask.shape[0] / 2), int(mask.shape[1] / 2))]

    # # Calculate image moments of the detected contour
    # M = cv2.moments(contours[0])
    # x = round(M['m10'] / M['m00'])
    # y = round(M['m01'] / M['m00'])
    # # center = (x, y)
    # center = (y, x)
    return centers
