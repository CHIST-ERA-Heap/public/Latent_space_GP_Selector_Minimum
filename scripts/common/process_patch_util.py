from PIL import Image, ImageFilter
import cv2
import numpy as np
# import imagehash

def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(img)

    # return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # try:
    #     return Image.fromarray(img)
    # except Exception:
    #     img = np.squeeze(img, axis=2)  # axis=2 is channel dimension
    #     return Image.fromarray(img)


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    return np.asarray(img)

def resize_pillow(cv2_image, resized_size_w, resized_size_h):
    cv2_image = convert_from_cv2_to_image(cv2_image)
    newsize = (int(resized_size_w), int(resized_size_h))
    cv2_image = cv2_image.resize(newsize)
    return convert_from_image_to_cv2(cv2_image)

def downsize_array(depth_array_input, target_size):
    nb_ligne,nb_colonne = target_size
    depth_array_resized = np.empty(target_size)

    for i in range (0,nb_ligne) :
        for j in range (0,nb_colonne):
            depth_array_resized [i][j] = (depth_array_input[2*i][2*j]+depth_array_input[2*i][2*j+1] + depth_array_input[2*i+1][2*j] + depth_array_input [2*i+1][2*j+1])/4
    return depth_array_resized

def mean_4pts(depth_array_input, h_r, w_r):
    depth_array_resized = np.empty((h_r, w_r))
    for i in range(0,h_r):
        for j in range(0, w_r):
            depth_array_resized[i][j]=(
                depth_array_input[2 * i][2 * j]+
                depth_array_input[2 * i][2 * j + 1]+
                depth_array_input[2 * i + 1][2 * j] +
                depth_array_input[2 * i + 1][2 * j + 1]
            )/4
    depth_array_resized = depth_array_resized.reshape((h_r, w_r, 1))
    return depth_array_resized


# def post_process_output(x, b_between_minus_1=True):
#     x = np.asarray(x, dtype=np.float32)
#
#     if b_between_minus_1:
#         x = np.uint8((x + 1) * 255 / 2)
#     else:
#         # print("post_process_output - x.shape: ", x.shape)
#         x = np.uint8(x * 255)
#
#     return x
#
# def pre_process_input(x, channels_number=1, b_between_minus_1=True):
#     # print("input pre-process x.shape: ", x.shape)
#     x = np.asarray(x, dtype=np.float32)
#     if np.max(x) > 1.0:
#         x = x.astype('float32') / 255.
#         np.clip(x, 0.0, 1.0)
#     if b_between_minus_1:
#         x = np.array(x, dtype=np.float32) * 2 - 1
#
#     if x.shape[-1] != channels_number:
#         x = x.reshape(x.shape + (channels_number,))
#     # print("output pre-process x.shape: ", x.shape)
#
#     return x

def get_rgb_patch(rgb_image, grasp_position_x, grasp_position_y, image_rotation_angle_degree, dim_grasp_patch=96,
                  resized_size=32, dim_grasp_patch_y=None):
    rows, cols, canal = rgb_image.shape
    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst1 = cv2.warpAffine(rgb_image, M, (cols, rows))
    dst1 = dst1[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
           int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    if resized_size is not None:
        # dst1 = cv2.resize(dst1, (resized_size, resized_size), interpolation=cv2.INTER_NEAREST)
        dst1 = resize_pillow(dst1, resized_size, resized_size)
    return dst1

def get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                        dim_grasp_patch=96, dim_grasp_patch_y=None, verbose = False):
    rows, cols, canal = depth_data.shape
    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    if verbose:
        print("dim_grasp_patch: ", dim_grasp_patch)
        print("DIM: ", DIM)
        print("DIM_y: ", DIM_y)
        print("depth_data.shape: ", depth_data.shape)

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst_d = cv2.warpAffine(depth_data, M, (cols, rows))
    dst_d = dst_d[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
            int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    # generate depth grayscale patch
    # cf autolab perception

    where_are_NaNs = np.isnan(dst_d)
    max_d = np.max(dst_d)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_are_NaNs] = max_d


    if verbose:
        print("dst_d.shape: ", dst_d.shape)
        print("mean dst_d: ", np.mean(dst_d))

    return dst_d  # raw depth

def get_SN_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree, dim_grasp_patch=96,
                       resized_size=32, max_depth_value=0.7):
    dst_d = get_grayscale_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                                      dim_grasp_patch, resized_size, max_depth_value=max_depth_value)
    np_array = np.asarray(dst_d)

    normal = get_SN(np_array)

    # cv2.imwrite(args.destination_folder+"bear_SN.png",normal)
    # image = Image.fromarray((normal).astype(np.uint8))

    return normal

def get_SN(np_array_depth_image, ksize=5, b_filter=False, b_cv2=True):  # parse args
    if b_filter:
        np_array_depth_image = Image.fromarray((np_array_depth_image.astype(np.uint8)))

        filter_size = 7

        size = np_array_depth_image.size
        f = 1 / 1
        np_array_depth_image = np_array_depth_image.resize((int(size[0] * f), int(size[1] * f)))
        np_array_depth_image = np_array_depth_image.resize((size[0], size[1]))

        # np_array = np_array.filter(ImageFilter.EDGE_ENHANCE())
        np_array_depth_image = np_array_depth_image.filter(ImageFilter.MedianFilter(filter_size))
        # np_array = np_array.filter(ImageFilter.EDGE_ENHANCE())

        # np_array = np_array.filter(ImageFilter.BLUR())
        # np_array = np_array.filter(ImageFilter.MedianFilter(3))
        # np_array = np_array.filter(ImageFilter.BLUR())
        # np_array = np_array.filter(ImageFilter.ModeFilter(3))
        # np_array = np_array.filter(ImageFilter.ModeFilter(3))
    np_array_depth_image = np.asarray(np_array_depth_image)

    # print("np_array_depth_image.shape, ", np_array_depth_image.shape)
    # print("max np_array_depth_image: ", np.max(np_array_depth_image))

    # h, w = np_array_depth_image.shape
    # normal = np.zeros((h, w, 3))

    zy, zx = np.gradient(np_array_depth_image)
    # You may also consider using Sobel to get a joint Gaussian smoothing and differentation
    # to reduce noise
    # zx = cv2.Sobel(np_array_depth_image, cv2.CV_64F, 1, 0, ksize=ksize)
    # zy = cv2.Sobel(np_array_depth_image, cv2.CV_64F, 0, 1, ksize=ksize)

    normal = np.dstack((-zx, -zy, np.ones_like(np_array_depth_image)))
    n = np.linalg.norm(normal, axis=2)
    normal[:, :, 0] /= n
    normal[:, :, 1] /= n
    normal[:, :, 2] /= n

    # print("max normal[:, :, 0]: ", np.max(normal[:, :, 0]))
    # print("max normal[:, :, 1]: ", np.max(normal[:, :, 1]))
    # print("max normal[:, :, 2]: ", np.max(normal[:, :, 2]))

    # offset and rescale values to be in 0-255
    normal += 1
    normal /= 2
    normal *= 255

    # print("max normal[:, :, 0]: ", np.max(normal[:, :, 0]))
    # print("max normal[:, :, 1]: ", np.max(normal[:, :, 1]))
    # print("max normal[:, :, 2]: ", np.max(normal[:, :, 2]))
    #
    # print("min normal[:, :, 0]: ", np.min(normal[:, :, 0]))
    # print("min normal[:, :, 1]: ", np.min(normal[:, :, 1]))
    # print("min normal[:, :, 2]: ", np.min(normal[:, :, 2]))
    #
    # print("mean normal[:, :, 0]: ", np.mean(normal[:, :, 0]))
    # print("mean normal[:, :, 1]: ", np.mean(normal[:, :, 1]))
    # print("mean normal[:, :, 2]: ", np.mean(normal[:, :, 2]))
    #
    # print("median normal[:, :, 0]: ", np.median(normal[:, :, 0]))
    # print("median normal[:, :, 1]: ", np.median(normal[:, :, 1]))
    # print("median normal[:, :, 2]: ", np.median(normal[:, :, 2]))

    # normal = np.array(d_im, dtype="float32")
    '''
    normal = np.zeros((d_im.shape[0], d_im.shape[1], 3), dtype="float16" )

    h,w = d_im.shape
    for i in range(1,w-1):
        for j in range(1,h-1):
            t = np.array([i,j-1,d_im[j-1,i]],dtype="float16")
            f = np.array([i-1,j,d_im[j,i-1]],dtype="float16")
            c = np.array([i,j,d_im[j,i]] , dtype = "float16")
            d = np.cross(f-c,t-c)
            n = d / np.sqrt((np.sum(d**2)))
            #print("normal.shape :", normal.shape)
            normal[j,i,:] = n

    '''

    if b_filter:
        normal = Image.fromarray((normal.astype(np.uint8)))

        size = normal.size
        f = 1 / 1
        normal = normal.resize((int(size[0] * f), int(size[1] * f)))
        normal = normal.resize((size[0], size[1]))

        # np_array = np_array.filter(ImageFilter.EDGE_ENHANCE())
        normal = normal.filter(ImageFilter.MedianFilter(filter_size))
        # np_array = np_array.filter(ImageFilter.EDGE_ENHANCE())

        # np_array = np_array.filter(ImageFilter.BLUR())
        # np_array = np_array.filter(ImageFilter.MedianFilter(3))
        # np_array = np_array.filter(ImageFilter.BLUR())
    normal = np.asarray(normal)

    if b_cv2:
        return normal[:, :, ::-1]
    else:
        return normal

def get_grayscale_depth_from_raw(dst_d, min_depth=None, max_depth=None, b_Xue=False, missing_value=0, b_invert=False, max_depth_value = 0.7, blur=False):#https://arxiv.org/pdf/1604.05817.pdf

    invalid_px = np.where(dst_d > max_depth_value)
    dst_d[invalid_px] = max_depth_value

    if b_Xue:
        # dst_d = np.reshape(dst_d, (dst_d.shape[0], dst_d.shape[1], 1))
        im_data = cv2.copyMakeBorder(dst_d, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
        mask = (im_data == missing_value).astype(np.uint8)

        # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
        scale = np.abs(im_data).max()
        # print('scale: ', scale)
        im_data = im_data.astype(np.float32) / scale  # Has to be float32, 64 not supported.
        im_data = cv2.inpaint(im_data, mask, 1, cv2.INPAINT_NS)

        im_data = im_data.astype(np.float32)
        im_data = np.reshape(im_data, (im_data.shape[0], im_data.shape[1], 1))

        # Back to original size and value range.
        im_data = im_data[1:-1, 1:-1]
        im_data = im_data * scale
        im_data = np.asarray(im_data)
        im_data = im_data.reshape((im_data.shape[0], im_data.shape[1], 1))
        # print("min im_data: ", np.min(im_data))
        # print("mean im_data: ", np.mean(im_data))
        # print("max im_data: ", np.max(im_data))

    else:
        twobyte = False
        normalize = True
        BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        max_val = BINARY_IM_MAX_VAL
        if twobyte:
            max_val = np.iinfo(np.uint16).max

        if normalize:
            min_depth = np.min(dst_d)
            max_depth = np.max(dst_d)
            depth_data = (dst_d - min_depth) / (max_depth - min_depth)
            depth_data = float(max_val) * depth_data.squeeze()
        else:
            zero_px = np.where(dst_d == 0)
            zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
            depth_data = ((dst_d - min_depth) * \
                          (float(max_val) / (max_depth - min_depth))).squeeze()
            depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        im_data = np.zeros([dst_d.shape[0], dst_d.shape[1], 3])
        im_data[:, :, 0] = depth_data
        im_data[:, :, 1] = depth_data
        im_data[:, :, 2] = depth_data
        im_data = im_data.astype(np.uint8)


       # generate depth grayscale patch
        # cf autolab perception

        # BINARY_IM_MAX_VAL = np.iinfo(np.uint8).max
        #
        # max_val = BINARY_IM_MAX_VAL
        #
        # min_depth = 0.0
        # # if min_depth is None:
        # #     min_depth = np.min(dst_d)
        #
        # if max_depth is None:
        #     max_depth = np.max(dst_d)
        #
        # depth_data = (dst_d - min_depth) / (max_depth - min_depth)
        # depth_data = float(max_val) * depth_data.squeeze()
        #
        # try:
        #     zero_px = np.where(dst_d == 0)
        #     zero_px = np.c_[zero_px[0], zero_px[1], zero_px[2]]
        # except(IndexError):
        #     pass
        #
        # depth_data = ((dst_d - min_depth) * \
        #               (float(max_val) / (max_depth - min_depth))).squeeze()
        # try:
        #     depth_data[zero_px[:, 0], zero_px[:, 1]] = 0
        # except(IndexError, TypeError):
        #     pass
        #
        # im_data = np.zeros([depth_data.shape[0], depth_data.shape[1], 3]).astype(np.uint8)
        # im_data[:, :, 0] = depth_data
        # im_data[:, :, 1] = depth_data
        # im_data[:, :, 2] = depth_data

        # if twobyte:
        #     return im_data.astype(np.uint16)
        # return im_data.astype(np.uint8)

        # depth_image = im_data.astype(np.uint8)
    if b_invert:
        im_data = 255.0 - im_data


    depth_image = im_data.astype(np.uint8)


    # print("depth_image.shape: ", depth_image.shape)
    if (depth_image.shape[2] == 3):
        depth_image = depth_image[:, :, 0]

    if blur:
        kernel = np.ones((5, 5), np.float32) / 25
        depth_image = cv2.filter2D(depth_image, -1, kernel)

    return depth_image

def get_grayscale_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                              dim_grasp_patch=96, resized_size=32, max_depth_value=0.7):
    dst_d = get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                                dim_grasp_patch)
    depth_image = get_grayscale_depth_from_raw(dst_d, max_depth_value=max_depth_value)
    # depth_image = cv2.resize(depth_image, (resized_size, resized_size), interpolation=cv2.INTER_NEAREST)
    depth_image = resize_pillow(depth_image, resized_size, resized_size)
    return depth_image

def ClipFloatValues(float_array, min_value, max_depth_value):
    """Clips values to the range [min_value, max_depth_value].

    First checks if any values are out of range and prints a message.
    Then clips all values to the given range.

    Args:
      float_array: 2D array of floating point values to be clipped.
      min_value: Minimum value of clip range.
      max_depth_value: Maximum value of clip range.

    Returns:
      The clipped array.

    """
    if float_array.min() < min_value or float_array.max() > max_depth_value:
        print('Image has out-of-range values [%f,%f] not in [%d,%d]',
              float_array.min(), float_array.max(), min_value, max_depth_value)
        float_array = np.clip(float_array, min_value, max_depth_value)
    return float_array

def FloatArrayToRgbImage(float_array,
                         scale_factor=256000.0,
                         drop_blue=False):
    """Convert a floating point array of values to an RGB image.

    Convert floating point values to a fixed point representation where
    the RGB bytes represent a 24-bit integer.
    R is the high order byte.
    B is the low order byte.
    The precision of the depth image is 1/256 mm.

    Floating point values are scaled so that the integer values cover
    the representable range of depths.

    This image representation should only use lossless compression.

    Args:
    float_array: Input array of floating point depth values in meters.
    scale_factor: Scale value applied to all float values.
    drop_blue: Zero out the blue channel to improve compression, results in 1mm
      precision depth values.

    Returns:
    24-bit RGB PIL Image object representing depth values.
    """
    # Scale the floating point array.
    scaled_array = np.floor(float_array * scale_factor + 0.5)
    # Convert the array to integer type and clip to representable range.
    min_inttype = 0
    max_inttype = 2 ** 24 - 1
    scaled_array = ClipFloatValues(scaled_array, min_inttype, max_inttype)
    int_array = scaled_array.astype(np.uint32)
    # Calculate:
    #   r = (f / 256) / 256  high byte
    #   g = (f / 256) % 256  middle byte
    #   b = f % 256          low byte
    rg = np.divide(int_array, 256)
    r = np.divide(rg, 256)
    g = np.mod(rg, 256)
    image_shape = int_array.shape
    rgb_array = np.zeros((image_shape[0], image_shape[1], 3), dtype=np.uint8)
    rgb_array[..., 0] = r
    rgb_array[..., 1] = g
    if not drop_blue:
        # Calculate the blue channel and add it to the array.
        b = np.mod(int_array, 256)
        rgb_array[..., 2] = b
    image_mode = 'RGB'
    image = Image.fromarray(rgb_array, mode=image_mode)
    return image

def get_24_bit_RGB_encoded_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                                       dim_grasp_patch=96, resized_size=32, b_use_raw=True, max_depth_value=0.7):
    if b_use_raw:
        np_array = get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                                       dim_grasp_patch)
    else:
        dst_d = get_grayscale_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                                          dim_grasp_patch, resized_size, max_depth_value=max_depth_value)
        np_array = Image.fromarray(dst_d)
        # d_im = d_im.convert('L')
        np_array = np.asarray(np_array)

    # print("np_array.shape: ", np_array.shape)
    np_array = np_array.reshape(np_array.shape[0], np_array.shape[1])
    image = FloatArrayToRgbImage(np_array)
    open_cv_image = np.array(image)

    if b_use_raw:
        # open_cv_image = cv2.resize(open_cv_image, (resized_size, resized_size), interpolation=cv2.INTER_NEAREST)
        open_cv_image = resize_pillow(open_cv_image, resized_size, resized_size)
    return open_cv_image  # raw depth


def extract_single_feature(rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, use_rgb, use_gray_depth, use_SN, dim_grasp_patch, dim_resized_patch, flipped=0, b_filter=False, max_depth_value=0.7):
    try:
    # if True:
        if use_rgb:
            r = get_rgb_patch(rgb_scene, grasp_position_x, grasp_position_y,
                                      angle_degrees,
                                      dim_grasp_patch=dim_grasp_patch,
                                      resized_size=dim_resized_patch)

        if use_gray_depth or use_SN:
            # grasp_position_x, grasp_position_y
            raw_depth = get_raw_depth_patch(depth_raw, grasp_position_x, grasp_position_y,
                                            angle_degrees, dim_grasp_patch=dim_grasp_patch)
            gray = get_grayscale_depth_from_raw(raw_depth, max_depth_value=max_depth_value)

            # gray = cv2.resize(gray, (dim_resized_patch, dim_resized_patch),
            #                               interpolation=cv2.INTER_NEAREST)
            gray = resize_pillow(gray, dim_resized_patch, dim_resized_patch)

        if use_SN:
            sn = get_SN(gray, b_filter=b_filter)


        if (use_rgb and (not use_gray_depth) and (not use_SN)):
            x = r
        if (use_gray_depth and (not use_rgb) and (not use_SN)):
            x = gray
            x = x.reshape((dim_resized_patch, dim_resized_patch, 1))

        if (use_SN and (not use_rgb) and (not use_gray_depth)):
            x = sn
        if (use_rgb and use_gray_depth and (not use_SN)):
            x = np.dstack((np.asarray(r), np.asarray(gray))).astype(np.float32)
        if (use_rgb and use_SN and (not use_gray_depth)):
            x = np.dstack((np.asarray(r), np.asarray(sn))).astype(np.float32)
        if (use_SN and use_gray_depth and (not use_rgb)):
            x = np.dstack((np.asarray(gray), np.asarray(sn))).astype(np.float32)
        if (use_SN and use_gray_depth and use_rgb):
            x = np.dstack((np.asarray(r), np.asarray(gray), np.asarray(sn))).astype(
                np.float32)

        if flipped > 0:
            x = np.fliplr(x)
            x = x[..., ::-1] - np.zeros_like(x)
        return x

    # todo auto compute dim_grasp_patch size
    except Exception as e:
        # print(e)
        # print('trying with ', str(dim_grasp_patch-1))
        if dim_grasp_patch <= 1:
            return None
        else:
            return extract_single_feature(rgb_scene=rgb_scene, depth_raw=depth_raw, grasp_position_x=grasp_position_x, grasp_position_y=grasp_position_y, angle_degrees=angle_degrees, use_rgb=use_rgb, use_gray_depth=use_gray_depth, use_SN=use_SN, dim_grasp_patch=dim_grasp_patch-1, dim_resized_patch=dim_resized_patch, flipped=flipped, max_depth_value=max_depth_value)

#old
# def extract_patch(dic_images_rgb, dic_depth_raw_data, pcd, grasp_position_x, grasp_position_y,
#                   image_rotation_angle_degree, size=128, final_size=128, b_filter=False, verbose=False, max_depth_value=0.7):
#
#     rgb_image = get_rgb_patch(dic_images_rgb[pcd], grasp_position_x, grasp_position_y,
#                               image_rotation_angle_degree,
#                               dim_grasp_patch=size,
#                               resized_size=final_size)
#
#     # grasp_position_x, grasp_position_y
#     raw_depth = get_raw_depth_patch(dic_depth_raw_data[pcd], grasp_position_x, grasp_position_y,
#                                     image_rotation_angle_degree, dim_grasp_patch=size)
#
#     gray_depth_image = get_grayscale_depth_from_raw(raw_depth, max_depth_value=max_depth_value)
#
#     # gray_depth_image = cv2.resize(gray_depth_image, (final_size, final_size),
#     #                               interpolation=cv2.INTER_NEAREST)
#     gray_depth_image = resize_pillow(gray_depth_image, final_size, final_size)
#
#
#     SN_image = get_SN(gray_depth_image, b_filter=b_filter)
#
#     x = np.dstack(
#         (np.asarray(rgb_image), np.asarray(gray_depth_image), np.asarray(SN_image))).astype(np.float32)
#     if verbose:
#         base_name = "./"+str(pcd)+"_"+str(grasp_position_x)+"_"+str(grasp_position_y)+"_"+image_rotation_angle_degree+"_"
#         # r = batch_x[:, :, :, 0:3]
#         # r = X[:, :, 0:3]
#         rgb_name = base_name + '_rgb.png'
#         print("saving ", rgb_name)
#         cv2.imwrite(rgb_name, rgb_image)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
#
#         # gray = batch_x[:, :, :, 3:4]
#         # gray = X[:, :, 3:4]
#
#         gray_name = base_name + '_gray_depth.png'
#         print("saving ", gray_name)
#         cv2.imwrite(gray_name, gray_depth_image)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
#
#         # sn = batch_x[:, :, :, 4:]
#         # sn = X[:, :, 4:]
#         sn_name = base_name + '_sn_depth.png'
#         print("saving ", sn_name)
#         cv2.imwrite(sn_name, SN_image)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
#
#     return x

def post_process_output(x, b_between_minus_1=True, imagenet=False):
    x = np.asarray(x, dtype=np.float32)
    if imagenet:
        # Apply featurewise_center to test-data with statistics from train data
        # generator_mean = [123.68, 116.779, 103.939]
        generator_mean = [103.939, 116.779, 123.68]

        x += generator_mean
        # # Apply featurewise_std_normalization to test-data with statistics from train data
        # x /= (generator.std + K.epsilon())
    else:
        if b_between_minus_1:
            x = np.uint8((x + 1) * 255 / 2)
        else:
            # print("post_process_output - x.shape: ", x.shape)
            x = np.uint8(x * 255)

    return x

def select_postprocessing_fct(preprocessing_type):
    if preprocessing_type == 'None':
        return postprocessing_None

    if preprocessing_type == 'imagenet':
        return postprocessing_imagenet

    if preprocessing_type == 'betweenminus1and1':
        return postprocessing_betweenminus1and1

    if preprocessing_type == 'between0and1':
        return postprocessing_between0and1

    if preprocessing_type == 'between0and255':
        return postprocessing_between0and255

    if preprocessing_type == 'average_hash':
        return postprocessing_None


    print("error postprocessing_type not implemented: ", preprocessing_type)
    return None

def select_preprocessing_fct(preprocessing_type):
    if preprocessing_type == 'None':
        return preprocessing_None

    if preprocessing_type == 'imagenet':
        return preprocessing_imagenet

    if preprocessing_type == 'betweenminus1and1':
        return preprocessing_betweenminus1and1

    if preprocessing_type == 'between0and1':
        return preprocessing_between0and1

    if preprocessing_type == 'between0and255':
        return preprocessing_between0and255

    # if preprocessing_type == 'average_hash':
    #     return preprocessing_average_hash

    print("error preprocessing_type not implemented: ", preprocessing_type)
    return None

# def preprocessing_average_hash(x):
#     x = np.asarray(x, dtype=np.uint8)
#     # print('x.shape: ', x.shape)
#     x_all = []
#     for curr_image in x:
#         # print('curr_image.shape: ', curr_image.shape)
#         curr_image = convert_from_cv2_to_image(curr_image)
#         x_all.append(imagehash.average_hash(curr_image))
#     # x_all = np.asarray(x_all, dtype=np.uint8)
#     return x_all

#works with pretrained rgb
def preprocessing_None(x):
    # x = np.asarray(x, dtype=np.float32)
    return x

def postprocessing_None(x):
    # x = np.asarray(x, dtype=np.float32)
    return x

#works with pretrained rgb
def preprocessing_imagenet(x):
    x = np.asarray(x, dtype=np.float32)
    # mean = [123.68, 116.779, 103.939]
    mean = [103.939, 116.779, 123.68, 127.5, 127.5, 127.5, 127.5, 127.5]
    # x -= mean
    shape = np.shape(x)
    if len(shape) == 3:
        for i in range(shape[-1]):
            x[i, :, :] -= mean[i]
            # x[1, :, :] -= mean[1]
            # x[2, :, :] -= mean[2]
    else:
        for i in range(shape[-1]):
            x[:, i, :, :] -= mean[i]
            # x[:, 0, :, :] -= mean[0]
            # x[:, 1, :, :] -= mean[1]
            # x[:, 2, :, :] -= mean[2]
    # print('preprocessing_imagenet: ', np.mean(x))

    return x

def postprocessing_imagenet(x):
    x = np.asarray(x, dtype=np.float32)
    print('postprocessing_imagenet: ', np.mean(x))

    # generator_mean = [123.68, 116.779, 103.939]
    mean = [103.939, 116.779, 123.68, 127.5, 127.5, 127.5, 127.5, 127.5]
    shape = np.shape(x)
    # x += mean
    if len(shape) == 3:
        for i in range(shape[-1]):
            x[i, :, :] += mean[i]
            # x[1, :, :] += mean[1]
            # x[2, :, :] += mean[2]
    else:
        for i in range(shape[-1]):
            x[:, i, :, :] += mean[i]
            # x[:, 1, :, :] += mean[1]
            # x[:, 2, :, :] += mean[2]

    return x


def preprocessing_between0and255(x):
    x = np.asarray(x, dtype=np.float32)
    x = x.astype('float32')
    x = np.clip(x, 0.0, 255.0)

    return x

def preprocessing_between0and1(x):
    x = np.asarray(x, dtype=np.float32)
    x = x.astype('float32') / 255.
    x = np.clip(x, 0.0, 1.0)

    return x


def postprocessing_between0and255(x):
    x = np.asarray(x, dtype=np.float32)
    x = np.clip(x, 0.0, 255.0)

    return x

def postprocessing_between0and1(x):
    # print('postprocessing_between0and1 x.shape: ', x.shape)
    x = np.asarray(x, dtype=np.float32)
    x = np.uint8(x * 255)
    x = np.clip(x, 0.0, 255.0)

    return x

def preprocessing_betweenminus1and1(x):
    x = np.asarray(x, dtype=np.float32)
    x = x.astype('float32') / 255.
    x = np.clip(x, 0.0, 1.0)
    x = np.array(x, dtype=np.float32) * 2 - 1

    return x

def postprocessing_betweenminus1and1(x):
    x = np.asarray(x, dtype=np.float32)
    x = np.uint8((x + 1) * 255 / 2)
    # np.clip(x, 0.0, 255.0)
    # print("np.max(x): ", np.max(x))
    # print("np.min(x): ", np.min(x))

    return x

#vgg is BGR
def pre_process_input(x, channels_number=1, b_between_minus_1=True, imagenet=False):
    # print("input pre-process x.shape: ", x.shape)
    x = np.asarray(x, dtype=np.float32)
    if imagenet:
        # Apply featurewise_center to test-data with statistics from train data
        # generator_mean = [123.68, 116.779, 103.939]
        generator_mean = [103.939, 116.779, 123.68]
        x -= generator_mean
        # # Apply featurewise_std_normalization to test-data with statistics from train data
        # x /= (generator.std + K.epsilon())
    else:
        if np.max(x) > 1.0:
            x = x.astype('float32') / 255.
            x = np.clip(x, 0.0, 1.0)
        if b_between_minus_1:
            x = np.array(x, dtype=np.float32) * 2 - 1

    if x.shape[-1] != channels_number:
        x = x.reshape(x.shape + (channels_number,))
    # print("output pre-process x.shape: ", x.shape)

    return x


def normalise(img):
    """
    Normalise the image by converting to float [0,1] and zero-centering
    """
    img = img.astype(np.float32) / 255.0
    img -= img.mean()
