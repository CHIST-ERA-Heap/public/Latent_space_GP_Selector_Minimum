from scripts.models.supervised_models.supervised_models_utils import select_classifier_class, select_object_classifier_class, select_length_regressor
from scripts.common.process_patch_util import select_preprocessing_fct, select_postprocessing_fct

def setup_feature_extraction_stuff(config):
    print("setup_feature_extraction_stuff")
    representation_class = None
    preprocessing = config.get('feature_extraction', 'preprocessing')
    preprocessing_function_vae = select_preprocessing_fct(preprocessing)
    postprocessing_function_vae = select_postprocessing_fct(preprocessing)
    representation_class_type = config.get('feature_extraction', 'representation_class')

    if representation_class_type == 'AE':
        from scripts.models.feature_extraction.AE_Class import AE_Class_RGBDSN
        representation_class = AE_Class_RGBDSN(config=config,
                                                    preprocessing_function=preprocessing_function_vae,
                                                    postprocessing_function=postprocessing_function_vae)

    elif representation_class_type == 'BVAE':
        from scripts.models.feature_extraction.BVAE_Class import BVAE_Class_RGBDSN
        representation_class = BVAE_Class_RGBDSN(config=config,
                                                      preprocessing_function=preprocessing_function_vae,
                                                      postprocessing_function=postprocessing_function_vae)
    elif representation_class_type == 'MAE':
        from scripts.models.feature_extraction.MAE_Class import MAE_Class_RGBDSN
        representation_class = MAE_Class_RGBDSN(config=config,
                                                     preprocessing_function=preprocessing_function_vae,
                                                     postprocessing_function=postprocessing_function_vae)

    elif representation_class_type == 'TL':
        from scripts.models.feature_extraction.Transfer_Learning_Class import Transfer_Learning_Class_RGBDSN
        representation_class = Transfer_Learning_Class_RGBDSN(config=config,
                                                                   preprocessing_function=preprocessing_function_vae,
                                                                   postprocessing_function=postprocessing_function_vae)

    elif representation_class_type == 'None':
        from scripts.models.feature_extraction.None_Class import None_Class_RGBDSN
        representation_class = None_Class_RGBDSN(config=config,
                                                      preprocessing_function=preprocessing_function_vae,
                                                      postprocessing_function=postprocessing_function_vae)
    return representation_class

def setup_classifier(config, representation_class=None):
    preprocessing_type_classifier = config.get('classifier', 'preprocessing')
    classifier_type = config.get("classifier", "type")
    Classifier = select_classifier_class(classifier_type)
    classifier = None
    preprocessing_function_classifier = select_representation_fct(preprocessing_type_classifier, representation_class)
    if Classifier is not None:
        if classifier_type == 'ft':
            classifier = Classifier(config=config, preprocessing_function=preprocessing_function_classifier,
                                         base=representation_class.base)
        else:
            classifier = Classifier(config=config, preprocessing_function=preprocessing_function_classifier)

        if classifier.trained_model is not None:
            classifier.load_model(classifier.trained_model)

    return classifier

def setup_length_regressor(config, representation_class=None, prior_length_function=None):
    regressor_type = config.get("length_regressor","type")
    preprocessing_type_length_regressor = config.get('length_regressor','preprocessing')
    preprocessing_function_length_regressor = select_representation_fct(preprocessing_type_length_regressor, representation_class)
    Length_Regressor = select_length_regressor(regressor_type)
    length_regressor = None
    if Length_Regressor is not None:
        length_regressor = Length_Regressor(config = config, preprocessing_function=preprocessing_function_length_regressor, prior_length_function=prior_length_function)

        if length_regressor.trained_model_length is not None:
            length_regressor.load_model(length_regressor.trained_model_length)

    return length_regressor

def setup_object_classifier(config, representation_class=None):
    object_classifier_type = config.get("grasp_object_classifier", "Classifier_type")
    if object_classifier_type is None:
        classifier_object = None
    else:
        Classifier = select_object_classifier_class(object_classifier_type)
        preprocessing_type_classifier_obj = config.get('classifier', 'preprocessing')
        preprocessing_function_classifier_obj = select_representation_fct(preprocessing_type_classifier_obj, representation_class)
        classifier_object = Classifier(config = config, preprocessing_function = preprocessing_function_classifier_obj)

        if classifier_object.trained_model is not None:
            classifier_object.load_model(classifier_object.trained_model)

    return classifier_object

#from grasp to representation used by the algorithm
def select_representation_fct(preprocessing_type, representation_class):
    print("select_representation_fct with ", preprocessing_type)

    if preprocessing_type != 'feature_extraction':
        return select_preprocessing_fct(preprocessing_type)
    else:
        if representation_class is not None:
            if preprocessing_type == 'feature_extraction':
                return representation_class.encode_candidates
