import gc
import math
import cv2
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau
from tensorflow.keras import backend as K
from tensorflow.keras.losses import mse, mae, huber
from shutil import copyfile
from scripts.models.common.model import get_vae_example, get_vae_model_yolo2, get_vae_model_vgg16, get_vae_model_vgg19, get_vae_model_default, get_vae_model_resnet50
from scripts.common.process_patch_util import extract_single_feature
from scripts.models.common.utils import LRTensorBoard, select_optimizer, \
    get_name_network
from scripts.models.common.data_generator import My_Custom_Generator_Scenes, My_Custom_Generator
from scripts.common.pickle_util import load_obj, save_obj
from pathlib import Path
from scripts.common.drawings_util import generate_image_with_grasps
import time
from random import randint
from tensorflow.keras.models import Sequential
import os
import numpy as np
tf.get_logger().setLevel('ERROR')


class BVAE_Class_RGBDSN:
    def __init__(self, config, preprocessing_function, postprocessing_function):
        self.config = config
        self.base = None
        self.preprocessing_function = preprocessing_function
        self.postprocessing_function = postprocessing_function
        self.preprocessing_function_type = self.config.get('feature_extraction', 'preprocessing')

        # args = parse_arguments(sys.argv[1:])[0]
        self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
        self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

        self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
        self.channels_number = self.config.getint('data_modalities', 'channels_number')
        self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')
        self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
        self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')

        # Dimensionality of latent space
        self.b_decoder = self.config.getboolean('feature_extraction', 'load_decoder')
        self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

        self.model_type = self.config.get('feature_extraction', 'model_type')
        self.encoder = None
        self.base = None
        self.decoder = None
        self.trained_model = self.config.get('feature_extraction', 'trained_model')
        self.new = 0

        self.pretrained_keras = self.config.get('feature_extraction', 'pretrained_keras')
        if self.pretrained_keras == "None":
            self.pretrained_keras = None

        if self.trained_model == "None":
            self.trained_model = None
            self.b_setup_vae = False
        else:
            self.b_setup_vae = True
        # else:
        #     # if not os.path.exists(self.trained_model):
        #     #     print(self.trained_model, " file does not exists")
        #     #     self.trained_model = None
        #     # else:
        #     self.b_setup_vae = True
        #     self.load_model()

        self.feature_dict_path = self.config.get('feature_extraction', 'feature_dict_path')
        if self.feature_dict_path == "None":
            print("computed vae encoding will not be saved")
            self.feature_dict_path = None
            self.encoded_mu_dict = {}
            self.encoded_var_dict = {}
        else:
            path = Path(self.feature_dict_path)
            path.mkdir(parents=True, exist_ok=True)
            self.mu_path = self.feature_dict_path + 'dict_mu_encoded.pkl'
            self.var_path = self.feature_dict_path + 'dict_var_encoded.pkl'
            if os.path.isfile(self.mu_path):
                self.encoded_mu_dict = load_obj(self.mu_path)
            else:
                self.encoded_mu_dict = {}

            if os.path.isfile(self.var_path):
                self.encoded_var_dict = load_obj(self.var_path)
            else:
                self.encoded_var_dict = {}

    def save_dicts(self):
        if self.new > 0:
            if self.feature_dict_path is not None:
                print("new: ", self.new)
                print("saving: ", self.mu_path)
                save_obj(self.encoded_mu_dict, self.mu_path)
                print("saving: ", self.var_path)
                save_obj(self.encoded_var_dict, self.var_path)
                self.new = 0
            else:
                print("can not save mu/vae dicts")

    # if you change preprocessing , adapt decoder
    def get_vae_model(self, weights=None, base=None, verbose=False):
        image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)
        b_untrainable = False

        #todo
        if weights is not None:
            b_untrainable = True


        if self.model_type == 'resnet50':
            return get_vae_model_resnet50(image_shape, self.projection_dim, weights=weights, b_untrainable=b_untrainable,
                                       preprocessing_function_type=self.preprocessing_function_type, base=base)

        if self.model_type == 'yolo2':
            return get_vae_model_yolo2(image_shape, self.projection_dim, n=5, preprocessing_function_type=self.preprocessing_function_type, base=base)

        if self.model_type == 'vgg16':
            # if weights is None:
            #     b_untrainable = False
            # else:
            #     b_untrainable = True
            return get_vae_model_vgg16(image_shape, self.projection_dim, weights=weights, b_untrainable=b_untrainable,
                                       preprocessing_function_type=self.preprocessing_function_type, base=base)
        if self.model_type == 'vgg19':
            # if weights is None:
            #     b_untrainable = False
            # else:
            #     b_untrainable = True
            return get_vae_model_vgg19(image_shape, self.projection_dim, weights=weights, b_untrainable=b_untrainable,
                                       preprocessing_function_type=self.preprocessing_function_type, base=base)

        if self.model_type == 'default':
            # return get_vae_model_default(image_shape, self.projection_dim, preprocessing_function_type=self.preprocessing_function_type, base=base)
            return get_vae_example(image_shape, self.projection_dim, preprocessing_function_type=self.preprocessing_function_type, base=base)

    def load_model(self):
        print("load bvae network ", self.model_type)
        # Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device = "cpu:0"
        else:
            device = "gpu:0"

        with tf.device(device):

            # config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.compat.v1.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            gpus = tf.config.list_physical_devices('GPU')
            if self.b_force_cpu:
                # Restrict TensorFlow to only use the first GPU
                try:
                    tf.config.set_visible_devices(gpus[0], 'GPU')
                    logical_gpus = tf.config.list_logical_devices('GPU')
                    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
                except RuntimeError as e:
                    # Visible devices must be set before GPUs have been initialized
                    print(e)

            model, _, _ = self.get_vae_model(weights=self.pretrained_keras)
            model.summary()

            if self.trained_model is not None:
                # Load VAE that was jointly trained with a
                # predictor returned from create_predictor()
                print('load_weights BVAE: ', self.trained_model, ' exists ',
                      os.path.isfile(self.trained_model + ".index"))

                model.load_weights(self.trained_model)
                self.encoder = model.get_layer('encoder')

            if self.b_decoder:
                self.decoder = model.get_layer('decoder')
            else:
                self.decoder = None

            del model

            # K.clear_session()
            gc.collect()

    def del_network(self):
        print("del bvae network")

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        gc.collect

        del self.encoder

        if self.b_decoder:
            del self.decoder
            self.decoder = None

        tf.reset_default_graph()
        # K._SESSION.close()
        # K._SESSION = None

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        # cuda.select_device(0)
        # cuda.close()
        gc.collect()

        self.encoder = None
        self.base = None

    def extract_single_feature(self, rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, flipped=0,
                               use_rgb=None, use_gray_depth=None, use_SN=None, dim_grasp_patch=None,
                               dim_resized_patch=None):
        # return extract_single_feature(rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, self.use_rgb, self.use_gray_depth, self.use_SN, self.b_filter, self.dim_grasp_patch, self.dim_resized_patch, flipped=flipped)
        if use_rgb is None:
            use_rgb = self.use_rgb
        if use_gray_depth is None:
            use_gray_depth = self.use_gray_depth
        if use_SN is None:
            use_SN = self.use_SN

        if dim_grasp_patch is None:
            dim_grasp_patch = self.dim_grasp_patch
        if dim_resized_patch is None:
            dim_resized_patch = self.dim_resized_patch

        return extract_single_feature(rgb_scene=rgb_scene, depth_raw=depth_raw, grasp_position_x=grasp_position_x,
                                      grasp_position_y=grasp_position_y, angle_degrees=angle_degrees, use_rgb=use_rgb,
                                      use_gray_depth=use_gray_depth, use_SN=use_SN,
                                      dim_grasp_patch=dim_grasp_patch, dim_resized_patch=dim_resized_patch,
                                      flipped=flipped, max_depth_value=self.max_depth_value)

    def encode_array(self, X):
        X = np.asarray(X)
        print('X.shape: ', X.shape)

        X = X.reshape((-1, self.dim_resized_patch, self.dim_resized_patch, self.channels_number))
        X = self.preprocessing_function(X)

        if self.encoder is None:
            print("encoder not setup in get_features :(")
            self.load_model()
        # print("encode_array X.shape: ", X.shape)

        if self.max_input_size is not None and X.shape[0] > self.max_input_size:
            n_splits = (X.shape[0] // self.max_input_size) + 1
            b_first = True
            for d in np.array_split(X, n_splits):
                if b_first:
                    # mu, var, z_temp = self.encoder.predict(d)
                    mu, var = self.encoder.predict(d)

                    b_first = False
                else:
                    # mu_temp, var_temp, z_temp = self.encoder.predict(d)
                    mu_temp, var_temp = self.encoder.predict(d)

                    # print("mu_temp.shape: ", mu_temp.shape)
                    # print("var_temp.shape: ", var_temp.shape)

                    mu = np.concatenate((mu, mu_temp), axis=0)
                    var = np.concatenate((var, var_temp), axis=0)

                    # print("mu.shape: ", mu.shape)
                    # print("var.shape: ", var.shape)
        else:
            # mu, var, z = self.encoder.predict(X)
            mu, var = self.encoder.predict(X)


        gc.collect()
        mu = np.array(mu).reshape((-1, self.projection_dim))
        var = np.array(var).reshape((-1, self.projection_dim))
        # mu = np.array(mu).reshape((-1,))
        # var = np.array(var).reshape((-1,))

        return mu, var

    # def encode_candidates_raw(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):
    #     return self.encode_candidates(grasp_candidates, dict_rgb, dict_depth, flipped, verbose)
    def encode_candidates_raw(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, base_name=None):
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]

        missing_X = []
        i = -1
        for g in grasp_candidates:
            # print('encode_candidates_raw: ', g)
            i += 1
            features = self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                         math.degrees(g[8]), flipped=flipped[i])
            if base_name is not None:
                self.save_path(features, base_name+str(i), ending='original.png')

            missing_X.append(features)
        return self.encode_array(missing_X)


    def encode_candidates(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):

        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]
        if self.feature_dict_path is None:
            missing_X = []
            i = -1
            for g in grasp_candidates:
                # print('encode_candidates g: ', g)
                i += 1
                missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                             math.degrees(g[8]), flipped=flipped[i]))
            return self.encode_array(missing_X)
        else:

            missing_X = []
            missing_keys = []
            i = -1
            for g in grasp_candidates:
                # print("encode_candidates: ", g)
                i += 1

                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str not in self.encoded_mu_dict:
                    # print("g: ", g)
                    # print("missing_keys key_str: ", key_str)
                    missing_keys.append(key_str)

                    missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                                 math.degrees(g[8]), flipped=flipped[i]))

            if len(missing_X) > 0:
                self.new += len(missing_X)
                # print('predicting  ', len(missing_keys), " missing grasps")
                mu_missing, var_missing = self.encode_array(missing_X)

                for i in range(0, len(missing_keys)):
                    key_str = missing_keys[i]
                    temp_mu = mu_missing[i, :]
                    temp_var = var_missing[i, :]
                    # print("temp_mu.shape: ", temp_mu.shape)
                    # print("temp_var.shape: ", temp_var.shape)

                    self.encoded_mu_dict[key_str] = temp_mu
                    self.encoded_var_dict[key_str] = temp_var

            mu = []
            var = []
            for g in grasp_candidates:
                # key_str = str(g[0]) + "_" + str(g[5])
                key_str = str(int(g[1])) + "_" + str(int(g[3])) + "_" + str(int(g[4])) + "_" + str(
                    math.degrees(g[8])) + "_" + str(flipped[i])
                if key_str in self.encoded_mu_dict and key_str in self.encoded_var_dict:
                    temp_mu = self.encoded_mu_dict[key_str]
                    temp_var = self.encoded_var_dict[key_str]
                    # print("temp_mu.shape: ", temp_mu.shape)
                    # print("temp_var.shape: ", temp_var.shape)

                    mu.append(temp_mu.reshape((self.projection_dim)))
                    var.append(temp_var.reshape((self.projection_dim)))

            # #todo remove?
            # if len(missing_X) > 1:
            #     self.save_dicts()

            mu = np.array(mu).reshape((-1, self.projection_dim))
            var = np.array(var).reshape((-1, self.projection_dim))
            if verbose and self.decoder is not None:
                for j in range(len(grasp_candidates)):
                    x_latent = mu[j, :]
                    self.save_decoded_patch(x_latent.reshape((-1, self.projection_dim)),
                                            base_name=self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(
                                                j))

                    generated = generate_image_with_grasps([grasp_candidates[j]],
                                                           dict_rgb[grasp_candidates[j][1]].copy(),
                                                           b_with_rectangles=True,
                                                           thickness_line=1)

                    cv2.imwrite(self.feature_dict_path + str(grasp_candidates[j][1]) + '_' + str(j) + "_full.png",
                                generated)

            return mu, var

    # def post_process_output(self, x, b_between_minus_1=True):
    #     return post_process_output(x,b_between_minus_1)


    def save_path(self, gen_data, base_name, ending='decoded.png'):
        if self.use_rgb:
            # r = batch_x[:, :, :, 0:3]
            r = gen_data[:, :, 0:3]
            rgb_name = base_name + '_rgb_'+ending
            print("saving ", rgb_name)
            cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        last_shape = gen_data.shape[-1]

        if last_shape == 7:
            if self.use_gray_depth:
                # gray = batch_x[:, :, :, 3:4]
                gray = gen_data[:, :, 3:4]
                gray_name = base_name + '_gray_depth_'+ending
                print("saving ", gray_name)
                cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

            if self.use_SN:
                # sn = batch_x[:, :, :, 4:]
                sn = gen_data[:, :, 4:]
                sn_name = base_name + '_sn_depth_'+ending
                print("saving ", sn_name)
                cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        elif last_shape == 4:
            if not self.use_rgb:
                if self.use_gray_depth:
                    # gray = batch_x[:, :, :, 3:4]
                    gray = gen_data[:, :, 0:1]
                    gray_name = base_name + '_gray_depth_'+ending
                    print("saving ", gray_name)
                    cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                if self.use_SN:
                    # sn = batch_x[:, :, :, 4:]
                    sn = gen_data[:, :, 2:]
                    sn_name = base_name + '_sn_depth_'+ending
                    print("saving ", sn_name)
                    cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
            else:
                if self.use_gray_depth:
                    # gray = batch_x[:, :, :, 3:4]
                    gray = gen_data[:, :, 3:4]
                    gray_name = base_name + '_gray_depth_'+ending
                    print("saving ", gray_name)
                    cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

        elif last_shape == 3 and not self.use_rgb:
            if self.use_SN:
                # r = batch_x[:, :, :, 0:3]
                sn = gen_data[:, :, 0:3]
                sn_name = base_name + '_sn_depth_'+ending
                print("saving ", sn_name)
                cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
        else:
            if self.use_gray_depth:
                # gray = batch_x[:, :, :, 3:4]
                gray = gen_data[:, :, 0:1]
                gray_name = base_name + '_gray_depth_'+ending
                print("saving ", gray_name)
                cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

    def save_decoded_patch(self, mu, base_name):
        if self.decoder is None:
            print("decoder not setup in save_decoded_patch :(")
            self.load_model()

        shape = mu.shape
        if len(shape) == 1:
            mu = np.reshape(mu, (-1, shape[0]))

        print('save_decoded_patch mu.shape: ', mu.shape)
        gen_data = self.decoder.predict(mu)
        print('1 save_decoded_patch gen_data.shape: ', gen_data.shape)

        gen_data = self.postprocessing_function(gen_data)
        gen_data = gen_data[0]
        print('2 save_decoded_patch gen_data.shape: ', gen_data.shape)

        self.save_path(gen_data, base_name)


    def get_base_from_bvae(self):
        model, _, _ = self.get_vae_model()
        model.summary()
        model.load_weights(self.pretrained_model)
        model = model.get_layer('encoder')
        model.summary()
        self.base = Sequential()
        to_remove = ['t_mean', 't_log_var', 'conv2d', 'conv2d_1', 'conv2d_2', 'conv2d_3']
        for layer in model.layers:  # go through until last layer
            print("layer.name: ", layer.name)
            if layer.name not in to_remove:
                self.base.add(layer)

        self.base.summary()
        return self.base

    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_scene_grasp, config_path,
            where_to_save_model):
        from tensorflow.python.framework.ops import disable_eager_execution
        disable_eager_execution()
        # np_config.enable_numpy_behavior()


        dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        channels_number = self.config.getint('data_modalities', 'channels_number')
        bottleneck = self.config.getint('feature_extraction', 'projection_dim')

        random_sizes_str = self.config.get('feature_extraction', 'random_sizes')
        random_sizes = random_sizes_str.split('\n')
        random_sizes = [int(s) for s in random_sizes]

        optimizer = self.config.get('feature_extraction', 'optimizer')
        base_learning_rate = self.config.getfloat('feature_extraction', 'base_learning_rate')
        reg = self.config.getfloat('feature_extraction', 'reg')

        number_of_scenes_per_batch = self.config.getint('feature_extraction', 'number_of_scenes_per_batch')
        number_of_labels_per_scenes = self.config.getint('feature_extraction', 'number_of_labels_per_scenes')

        decay_factor = self.config.getfloat('feature_extraction', 'decay_factor')
        scheduler_patience = self.config.getint('feature_extraction', 'scheduler_patience')
        max_epochs = self.config.getint('feature_extraction', 'max_epochs')
        validation_split = self.config.getfloat('feature_extraction', 'validation_split')
        random_seed = self.config.getint('feature_extraction', 'random_seed')
        use_rgb = self.config.getboolean('data_modalities', 'use_rgb')
        use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        use_SN = self.config.getboolean('data_modalities', 'use_SN')

        network_name = get_name_network(self.model_type, use_rgb, use_gray_depth, use_SN, reg, channels_number, bottleneck, dim_resized_patch, random_sizes)

        self.pretrained_model = self.config.get('feature_extraction', 'pretrained_model')
        if self.pretrained_model == "None":
            self.pretrained_model = None

        model, t_mean, t_log_var = self.get_vae_model(verbose=True)

        if self.pretrained_model is not None:
            try:
                # Load VAE that was jointly trained with a
                model.load_weights(self.pretrained_model)
            except:
                self.base = self.get_base_from_bvae()
                model, t_mean, t_log_var = self.get_vae_model(verbose=True)

        print('base_learning_rate: ', base_learning_rate)
        optimizer = select_optimizer(optimizer=optimizer, base_learning_rate=base_learning_rate)
        # loss_list = []
        # loss_list.append(vae_loss)
        # loss_weights_list = []
        # loss_weights_list.append(1.0)
        # model.compile(optimizer=optimizer,
        #               loss=vae_loss, experimental_run_tf_function=False, monitor)



        grasps = []
        for key in dict_scene_grasp.keys():
            # print("key: ", key)
            for g in dict_scene_grasp[key]:
                # print("adding ", g)
                grasps.append(g)

        X_train_grasps, X_val_grasps = train_test_split(grasps, test_size=validation_split,
                                                        random_state=random_seed)
        print('len X_train_grasps: ', len(X_train_grasps))
        print('len X_val_grasps: ', len(X_val_grasps))

        allow_flip = self.config.getboolean('feature_extraction', 'use_augmentation')
        if allow_flip:
            random_angle_augmentation_degrees = [i for i in range(-180, 181, 1)]
        else:
            random_angle_augmentation_degrees = [0.0]

        if number_of_scenes_per_batch == -1:
            number_of_scenes_per_batch = 1
            number_of_scenes_per_batch_val = 1
            my_training_batch_generator = My_Custom_Generator(X_train_grasps, number_of_labels_per_scenes,
                                                              channels_number=channels_number,
                                                              allow_flip=allow_flip,
                                                              preprocessing_fct=self.preprocessing_function,
                                                              random_angles=random_angle_augmentation_degrees,
                                                              final_size=dim_resized_patch,
                                                              random_sizes=random_sizes,
                                                              dic_images_rgb=dict_rgb_scene,
                                                              dic_depth_raw_data=dict_depth_scene,
                                                              use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                              use_SN=use_SN,
                                                              b_remember_patch=False, max_depth_value=self.max_depth_value)

            number_of_labels_per_scenes_val = number_of_labels_per_scenes
            my_validation_batch_generator = My_Custom_Generator(X_val_grasps, number_of_labels_per_scenes_val,
                                                                channels_number=channels_number,
                                                                preprocessing_fct=self.preprocessing_function,
                                                                allow_flip=False,
                                                                random_angles=[0.0],
                                                                final_size=dim_resized_patch,
                                                                random_sizes=random_sizes,
                                                                dic_images_rgb=dict_rgb_scene,
                                                                dic_depth_raw_data=dict_depth_scene,
                                                                use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                use_SN=use_SN,
                                                                b_remember_patch=False, verbose=False, max_depth_value=self.max_depth_value)

            training_steps = len(my_training_batch_generator)
            validation_steps = len(my_validation_batch_generator)
        else:
            X_train_grasps_dict = {}
            for g in X_train_grasps:
                # print("g: ", g)
                if g[1] not in X_train_grasps_dict:
                    X_train_grasps_dict[g[1]] = []
                X_train_grasps_dict[g[1]].append(g)

            X_val_grasps_dict = {}
            for g in X_val_grasps:
                if g[1] not in X_val_grasps_dict:
                    X_val_grasps_dict[g[1]] = []
                X_val_grasps_dict[g[1]].append(g)

            my_training_batch_generator = My_Custom_Generator_Scenes(X_train_grasps_dict, number_of_labels_per_scenes,
                                                                     number_of_scenes_per_batch,
                                                                     allow_flip=allow_flip,
                                                                     channels_number=channels_number,
                                                                     preprocessing_fct=self.preprocessing_function,
                                                                     random_angles=random_angle_augmentation_degrees,
                                                                     final_size=dim_resized_patch,
                                                                     random_sizes=random_sizes,
                                                                     dic_images_rgb=dict_rgb_scene,
                                                                     dic_depth_raw_data=dict_depth_scene,
                                                                     use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                     use_SN=use_SN,
                                                                     b_remember_patch=False, max_depth_value=self.max_depth_value)
            training_steps = my_training_batch_generator.n
            number_of_labels_per_scenes_val = int(number_of_labels_per_scenes * 0.3)
            if number_of_labels_per_scenes_val > len(X_val_grasps):
                number_of_labels_per_scenes_val = len(X_val_grasps)
            if number_of_labels_per_scenes_val == 0:
                number_of_labels_per_scenes_val = 1
            print('number_of_labels_per_scenes_val: ', number_of_labels_per_scenes_val)
            number_of_scenes_per_batch_val = int(number_of_scenes_per_batch*number_of_labels_per_scenes/number_of_labels_per_scenes_val)
            print('number_of_scenes_per_batch_val: ', number_of_scenes_per_batch_val)
            my_validation_batch_generator = My_Custom_Generator_Scenes(X_val_grasps_dict,
                                                                       number_of_labels_per_scenes_val,
                                                                       number_of_scenes_per_batch_val,
                                                                       allow_flip=False,
                                                                       channels_number=channels_number,
                                                                       preprocessing_fct=self.preprocessing_function,
                                                                       random_angles=[0.0],
                                                                       final_size=dim_resized_patch,
                                                                       random_sizes=random_sizes,
                                                                       dic_images_rgb=dict_rgb_scene,
                                                                       dic_depth_raw_data=dict_depth_scene,
                                                                       use_rgb=use_rgb, use_gray_depth=use_gray_depth,
                                                                       use_SN=use_SN,
                                                                       b_remember_patch=False, verbose=False, max_depth_value=self.max_depth_value)
            validation_steps = int(my_validation_batch_generator.n/number_of_scenes_per_batch_val)
            if validation_steps == 0:
                validation_steps = 1

        #

        timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())

        save_dir = where_to_save_model
        dir_path = save_dir + "/" + timestamp + "/"


        tb = TensorBoard(log_dir=dir_path + "/", write_graph=True, update_freq='epoch', profile_batch = 100000000)

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

        copyfile(config_path, dir_path + "config.ini")

        print("saving: X_train_grasps")
        save_obj(X_train_grasps, dir_path + "X_train_grasps.pkl")

        print("saving: X_val_grasps")
        save_obj(X_val_grasps, dir_path + "X_val_grasps.pkl")

        save_file = os.path.join(dir_path, network_name)

        checkpoint = ModelCheckpoint(filepath=save_file, monitor='val_loss',
                                     verbose=0, save_best_only=True, save_weights_only=True, mode='auto')

        # lr_schedule = ReduceLROnPlateau(monitor='val_loss', factor=decay_factor,
        #                                 mode='min', patience=scheduler_patience / 2, min_lr=1e-08, min_delta=10.0)

        total_steps = training_steps * max_epochs
        warmup_steps = int(total_steps * 0.15)

        class WarmUpCosine(tf.keras.optimizers.schedules.LearningRateSchedule):
            def __init__(
                    self, learning_rate_base, total_steps, warmup_learning_rate, warmup_steps
            ):
                super(WarmUpCosine, self).__init__()

                self.learning_rate_base = learning_rate_base
                self.total_steps = total_steps
                self.warmup_learning_rate = warmup_learning_rate
                self.warmup_steps = warmup_steps
                self.pi = tf.constant(np.pi)

            def get_config(self):
                config = {
                    'learning_rate_base': self.learning_rate_base,
                    'total_steps': self.total_steps,
                    'warmup_learning_rate': self.warmup_learning_rate,
                    # 'pi': self.pi,
                    'warmup_steps': self.warmup_steps,
                }
                return config

            def __call__(self, step):
                if self.total_steps < self.warmup_steps:
                    raise ValueError("Total_steps must be larger or equal to warmup_steps.")

                cos_annealed_lr = tf.cos(
                    self.pi
                    * (tf.cast(step, tf.float32) - self.warmup_steps)
                    / float(self.total_steps - self.warmup_steps)
                )
                learning_rate = 0.5 * self.learning_rate_base * (1 + cos_annealed_lr)

                if self.warmup_steps > 0:
                    if self.learning_rate_base < self.warmup_learning_rate:
                        raise ValueError(
                            "Learning_rate_base must be larger or equal to "
                            "warmup_learning_rate."
                        )
                    slope = (
                                    self.learning_rate_base - self.warmup_learning_rate
                            ) / self.warmup_steps
                    warmup_rate = slope * tf.cast(step, tf.float32) + self.warmup_learning_rate
                    learning_rate = tf.where(
                        step < self.warmup_steps, warmup_rate, learning_rate
                    )
                return tf.where(
                    step > self.total_steps, 0.0, learning_rate, name="learning_rate"
                )

        # scheduled_lrs = WarmUpCosine(
        #     learning_rate_base=base_learning_rate,
        #     total_steps=total_steps,
        #     warmup_learning_rate=0.0,
        #     warmup_steps=warmup_steps,
        # )
        WEIGHT_DECAY = 1e-4
        # optimizer = tfa.optimizers.AdamW(learning_rate=base_learning_rate, weight_decay=WEIGHT_DECAY)

        def fc_rc_loss(x, t_decoded, type='mse'):
            if type == 'mae':
                return mae(K.flatten(t_decoded), K.flatten(x))
            elif type == 'mse':
                # return tf.reduce_mean((x - t_decoded) ** 2)
                return K.sum(K.mean(K.square(t_decoded - x), axis=-1))
                # return K.sum(K.square(t_decoded - x), axis=-1)
                # return mse(K.flatten(t_decoded), K.flatten(x))
            else:
                return K.sum(K.binary_crossentropy(
                    K.batch_flatten(x),
                    K.batch_flatten(t_decoded)), axis=-1)

        def fc_kl_loss(x, t_decoded):
            return -0.5 * K.sum(1 + t_log_var \
                                - K.square(t_mean) \
                                - K.exp(t_log_var), axis=-1)

            # kl_loss = 1 + t_log_var - K.square(t_mean) - K.exp(t_log_var)
            # kl_loss = K.sum(kl_loss, axis=-1)
            # kl_loss *= -0.5
            # return kl_loss

        # Compute VAE loss
        def vae_loss(x, t_decoded):
            '''
            Negative variational lower bound used as loss function for
            training the variational autoencoder on the MNIST dataset.
            '''
            # Reconstruction loss
            rc_loss = fc_rc_loss(x, t_decoded)
            # rc_loss = fc_ssim_loss(x, t_decoded) #TODO check

            # Regularization term (KL divergence)
            kl_loss = fc_kl_loss(x, t_decoded)
            kl_loss = reg * kl_loss

            return K.mean(rc_loss + kl_loss)

        model.compile(
            # optimizer=optimizer, loss=MeanSquaredError(), metrics=["mae"], run_eagerly=True
            optimizer=optimizer, loss=vae_loss, metrics=["mae"], run_eagerly=False

        )


        lr_monitor = LRTensorBoard(log_dir=dir_path + "/")

        # tr = TrainMonitor(dir_path + "/", epoch_interval=5),

        # lr_early_stopping = EarlyStoppingByLrVal(value=1e-08, min_delta=10.0)
        lr_early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min',
                                                             patience=scheduler_patience,
                                                             restore_best_weights=True, min_delta=1.0)
        test_features, _ = my_validation_batch_generator[0]
        class TrainMonitor(tf.keras.callbacks.Callback):
            def __init__(self, path_to_save_dir, use_rgb, use_gray_depth, use_SN, epoch_interval=None, post_process_fct=None):
                self.epoch_interval = epoch_interval
                self.path_to_save_dir = path_to_save_dir
                self.post_process_fct=post_process_fct
                self.use_rgb = use_rgb
                self.use_gray_depth = use_gray_depth
                self.use_SN = use_SN

            def save_image(self, gen_data, base_name):
                if self.use_rgb:
                    # r = batch_x[:, :, :, 0:3]
                    r = gen_data[:, :, 0:3]
                    rgb_name = base_name + '_rgb.png'
                    print("saving ", rgb_name)
                    cv2.imwrite(rgb_name, r)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                last_shape = gen_data.shape[-1]

                if last_shape == 7:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 3:4]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                    if self.use_SN:
                        # sn = batch_x[:, :, :, 4:]
                        sn = gen_data[:, :, 4:]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                elif last_shape == 4:
                    if not self.use_rgb:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 0:1]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

                        if self.use_SN:
                            # sn = batch_x[:, :, :, 4:]
                            sn = gen_data[:, :, 2:]
                            sn_name = base_name + '_sn_depth.png'
                            print("saving ", sn_name)
                            cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                    else:
                        if self.use_gray_depth:
                            # gray = batch_x[:, :, :, 3:4]
                            gray = gen_data[:, :, 3:4]
                            gray_name = base_name + '_gray_depth.png'
                            print("saving ", gray_name)
                            cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                elif last_shape == 3 and not self.use_rgb:
                    if self.use_SN:
                        # r = batch_x[:, :, :, 0:3]
                        sn = gen_data[:, :, 0:3]
                        sn_name = base_name + '_sn_depth.png'
                        print("saving ", sn_name)
                        cv2.imwrite(sn_name, sn)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))
                else:
                    if self.use_gray_depth:
                        # gray = batch_x[:, :, :, 3:4]
                        gray = gen_data[:, :, 0:1]
                        gray_name = base_name + '_gray_depth.png'
                        print("saving ", gray_name)
                        cv2.imwrite(gray_name, gray)  # cv2.cvtColor(result, cv2.COLOR_RGB2BGR))

            def on_epoch_end(self, epoch, logs=None):
                if self.epoch_interval and epoch % self.epoch_interval == 0:
                    # np_config.enable_numpy_behavior()
                    # test_augmeneted_images = self.model.test_augmentation_model(test_features)
                    test_augmeneted_images = test_features
                    test_decoder_outputs = model.predict(test_augmeneted_images)

                    idx = randint(0, len(test_augmeneted_images) - 1)
                    print(f"\nIdx chosen: {idx}")
                    original_image = test_augmeneted_images[idx]

                    reconstructed_image = test_decoder_outputs[idx]

                    # self.path_to_save_dir
                    #
                    # original_brg = cv2.cvtColor(np.asarray(original_image*255.0), cv2.COLOR_BGR2RGB)
                    # masked_image_bgr = cv2.cvtColor(np.asarray(masked_image*255.0), cv2.COLOR_BGR2RGB)
                    # reconstructed_image_bgr = cv2.cvtColor(np.asarray(reconstructed_image*255.0), cv2.COLOR_BGR2RGB)
                    original_brg = self.post_process_fct(original_image)
                    base_name = self.path_to_save_dir+str(epoch)+'_original_'
                    self.save_image(original_brg,base_name )
                    # masked_image_bgr = np.asarray(masked_image*255.0)
                    gen_data = self.post_process_fct(reconstructed_image)
                    base_name = self.path_to_save_dir+str(epoch)+'_reconstructed_'
                    self.save_image(gen_data,base_name )


                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_original.png', original_brg)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_masked.png', masked_image_bgr)
                    # cv2.imwrite(self.path_to_save_dir+str(epoch)+'_reconstructed.png', reconstructed_image_bgr)



                    # fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5))
                    # # ax[0].imshow(original_image)
                    # ax[0].imshow(cv2.cvtColor(np.asarray(original_image), cv2.COLOR_BGR2RGB))
                    # ax[0].set_title(f"Original: {epoch:03d}")
                    #
                    # # ax[1].imshow(masked_image)
                    # ax[1].imshow(cv2.cvtColor(np.asarray(masked_image), cv2.COLOR_BGR2RGB))
                    # ax[1].set_title(f"Masked: {epoch:03d}")
                    #
                    # # ax[2].imshow(reconstructed_image)
                    # ax[2].imshow(cv2.cvtColor(np.asarray(reconstructed_image), cv2.COLOR_BGR2RGB))
                    # ax[2].set_title(f"Resonstructed: {epoch:03d}")
                    #
                    # plt.show()
                    # plt.close()

        tm = TrainMonitor(dir_path + "/", self.use_rgb, self.use_gray_depth, self.use_SN, epoch_interval=1, post_process_fct=self.postprocessing_function)

        lr_schedule = ReduceLROnPlateau(monitor='val_loss', factor=decay_factor,
                                        mode='min', patience=scheduler_patience / 2, min_lr=base_learning_rate*0.01, min_delta=10.0)

        train_callbacks = [tb, checkpoint,  lr_monitor, lr_schedule, lr_early_stopping, tm]

        for l in model.layers:
            print(l.name, l.trainable)

        print("number_of_labels_per_scenes: ", number_of_labels_per_scenes)
        print("my_training_batch_generator.n: ", my_training_batch_generator.n)
        # training_steps = math.ceil(my_training_batch_generator.n // number_of_scenes_per_batch)

        print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)

        model.fit(x=my_training_batch_generator,
                                # batch_size=number_of_labels_per_scenes*number_of_scenes_per_batch,
                                epochs=max_epochs,
                                steps_per_epoch=training_steps,
                                shuffle=True,
                                validation_data=my_validation_batch_generator,
                                validation_steps=validation_steps,
                                # validation_batch_size=number_of_labels_per_scenes_val*number_of_scenes_per_batch_val,
                                callbacks=train_callbacks,
                                verbose=1)