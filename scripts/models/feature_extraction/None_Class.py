import math
import gc
from scripts.common.process_patch_util import extract_single_feature
import numpy as np

class None_Class_RGBDSN:
    def __init__(self, config, preprocessing_function, postprocessing_function):
        self.config = config
        self.base = None
        self.preprocessing_function = preprocessing_function
        self.postprocessing_function = postprocessing_function
        self.preprocessing_function_type = self.config.get('feature_extraction', 'preprocessing')

        # args = parse_arguments(sys.argv[1:])[0]
        self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
        self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

        self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
        self.channels_number = self.config.getint('data_modalities', 'channels_number')
        self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')
        self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
        self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')

        self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

        self.model_type = self.config.get('feature_extraction', 'model_type')
        self.trained_model = self.config.get('feature_extraction', 'trained_model')
        self.new = 0

        if self.trained_model == "None":
            self.trained_model = None

        # if self.trained_model is None:
        #     print('None_Class_RGBDSN not setup correctly')
        self.feature_extraction_model = True
        self.feature_dict_path = None
        self.encoded_mu_dict = {}

        # self.feature_dict_path = self.config.get('feature_extraction', 'feature_dict_path')
        # if self.feature_dict_path == "None":
        #     print("computed encoding will not be saved")
        #     self.feature_dict_path = None
        #     self.encoded_mu_dict = {}
        # else:
        #     path = Path(self.feature_dict_path)
        #     path.mkdir(parents=True, exist_ok=True)
        #     self.encoded_path = self.feature_dict_path + 'dict_encoded.pkl'
        #     if os.path.isfile(self.encoded_path):
        #         self.encoded_mu_dict = load_obj(self.encoded_path)
        #     else:
        #         self.encoded_mu_dict = {}
        self.encoded_var_dict = {}

        self.load_model()

    def save_dicts(self):
        pass

    def get_none_model(self, verbose=False):
        pass

    def load_model(self):
        pass


    def del_network(self):
        pass

    def extract_single_feature(self, rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, flipped=0,
                               use_rgb=None, use_gray_depth=None, use_SN=None, dim_grasp_patch=None,
                               dim_resized_patch=None):
        # return extract_single_feature(rgb_scene, depth_raw, grasp_position_x, grasp_position_y, angle_degrees, self.use_rgb, self.use_gray_depth, self.use_SN, self.b_filter, self.dim_grasp_patch, self.dim_resized_patch, flipped=flipped)
        if use_rgb is None:
            use_rgb = self.use_rgb
        if use_gray_depth is None:
            use_gray_depth = self.use_gray_depth
        if use_SN is None:
            use_SN = self.use_SN
        if dim_grasp_patch is None:
            dim_grasp_patch = self.dim_grasp_patch
        if dim_resized_patch is None:
            dim_resized_patch = self.dim_resized_patch

        return extract_single_feature(rgb_scene=rgb_scene, depth_raw=depth_raw, grasp_position_x=grasp_position_x,
                                      grasp_position_y=grasp_position_y, angle_degrees=angle_degrees, use_rgb=use_rgb,
                                      use_gray_depth=use_gray_depth, use_SN=use_SN,
                                      dim_grasp_patch=dim_grasp_patch, dim_resized_patch=dim_resized_patch,
                                      flipped=flipped, max_depth_value=self.max_depth_value)


    def encode_array(self, X, b_raw=False):
        X = np.asarray(X)
        X = X.reshape((-1, self.dim_resized_patch, self.dim_resized_patch, self.channels_number))
        X = self.preprocessing_function(X)

        # if self.feature_extraction_model is None:
        #     print("encoder not setup in get_features :(")
        #     self.load_model()
        # print("encode_array X.shape: ", X.shape)

        gc.collect()
        # encoded = np.array(encoded).reshape((-1, self.projection_dim))

        return X


    def encode_candidates_raw(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]

        missing_X = []
        i = -1
        for g in grasp_candidates:
            # print('encode_candidates_raw: ', g)
            i += 1
            missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                         math.degrees(g[8]), flipped=flipped[i]))
        return self.encode_array(missing_X, b_raw=True), None


    def encode_candidates(self, grasp_candidates, dict_rgb, dict_depth, flipped=None, verbose=False):
        # print('None_Class_RGBDSN encode_candidates: ')
        if flipped is None:
            flipped = [0 for i in range(len(grasp_candidates))]
        # if self.feature_dict_path is None:
        missing_X = []
        i = -1
        for g in grasp_candidates:
            i += 1
            missing_X.append(self.extract_single_feature(dict_rgb[g[1]], dict_depth[g[1]], g[3], g[4],
                                                         math.degrees(g[8]), flipped=flipped[i]))

        r = self.encode_array(missing_X)
        # print('None_Class_RGBDSN encode_candidates: ', r.shape)
        return r, np.zeros_like(r)

    def save_decoded_patch(self, mu, base_name):
        print('no decoder')
        return