import gc
from tensorflow.keras import backend as K
import time
import math
import tensorflow as tf

from scripts.models.common.data_generator import My_Custom_Generator, My_Custom_Generator_bvae
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau
from scripts.models.common.utils import LRTensorBoard, select_optimizer, \
    get_name_network
from scripts.models.common.model import create_binary_classifier_yolo2, create_binary_classifier_vgg16, create_binary_classifier_small, create_binary_resnet50, get_vae_model_yolo2, get_vae_model_vgg16, create_binary_classifier_with_base, create_multilabel_classifier_small, create_multilabel_classifier_with_base
from scripts.common.process_patch_util import extract_single_feature
import numpy as np
import os
from shutil import copyfile
from scripts.common.pickle_util import save_obj
from tensorflow.keras.models import Sequential

from tensorflow.python.framework import ops

class CNNClassifier:

    def __init__(self,config, preprocessing_function, nb_of_class=2, verbose=False):
        self.config = config
        self.nb_of_class = nb_of_class
        self.preprocessing_function = preprocessing_function
        self.preprocessing_type_classifier = self.config.get('classifier', 'preprocessing')

        self.bottleneck = self.config.getint('feature_extraction', 'bottleneck')
        self.dim_resized_patch = self.config.getint('feature_extraction', 'dim_resized_patch')
        self.channels_number = self.config.getint('feature_extraction', 'channels_number')
        self.use_rgb = self.config.getboolean('feature_extraction', 'use_rgb')
        self.use_gray_depth = self.config.getboolean('feature_extraction', 'use_gray_depth')
        self.use_SN = self.config.getboolean('feature_extraction', 'use_SN')
        self.use_SN_filter = self.config.getboolean('feature_extraction', 'use_SN_filter')
        self.min_lr = self.config.getfloat('classifier','min_lr')


        random_sizes_str = self.config.get('bvae_training', 'random_sizes')
        random_sizes = random_sizes_str.split('\n')
        self.random_sizes = [int(s) for s in random_sizes]

        self.optimizer = self.config.get('bvae_training', 'optimizer')
        self.base_learning_rate = self.config.getfloat('bvae_training', 'base_learning_rate')
        self.train_batch_size = self.config.getint('classifier', 'max_batch_size')
        self.validation_batch_size = self.config.getint('classifier', 'max_batch_size')
        self.by_object = self.config.getfloat('classifier','by_object')

        self.decay_factor = self.config.getfloat('bvae_training', 'decay_factor')
        self.max_epochs = self.config.getint('bvae_training', 'max_epochs')
        self.allow_flip = self.config.getboolean('classifier', 'use_augmentation')

        self.max_epoch_without_improvement = self.config.getint('classifier', 'max_epoch_without_improvement')
        self.scheduler_patience = self.config.getint('classifier', 'scheduler_patience')

        self.type = config.get('classifier', 'CNNClassifier_type')
        self.pretrained_keras = config.get('classifier', 'pretrained_keras')
        if self.pretrained_keras == "None":
            self.pretrained_keras = None

        self.model = None

        self.b_force_cpu = not self.config.getboolean('classifier', 'use_gpu')
        self.b_with_augmentation = self.config.getboolean('classifier','use_augmentation')

        self.trained_model = self.config.get('classifier', 'trained_model')
        if self.trained_model == "None":
            self.trained_model = None

        self.transferweights_classifier = self.config.get('classifier', 'transferweights_classifier')
        if self.transferweights_classifier == "None":
            self.transferweights_classifier = None

        self.dict_sqlid_categorical_id = {}

    #if you change preprocessing , adapt decoder
    def get_vae_model(self, verbose=False):
        image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)

        bottleneck = self.config.getint('feature_extraction','bottleneck')

        if self.type == 'yolo2':
            return get_vae_model_yolo2(image_shape, bottleneck, n=5, preprocessing_function_type=self.preprocessing_type_classifier, verbose=verbose)
        if self.type == 'vgg16':
            return get_vae_model_vgg16(image_shape, bottleneck, weights=None, b_untrainable=False, preprocessing_function_type=self.preprocessing_type_classifier, verbose=verbose)

    def load_vae_model(self):
        print("load bvae network as CNN transfer learning", self.type)
        #Failed to memcopy into scratch buffer for device
        # tf.config.experimental.list_physical_devices(device_type=None)
        if self.b_force_cpu:
            device="cpu:0"
        else:
            device="gpu:0"

        with tf.device(device):
            # config = tf.ConfigProto(allow_soft_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.3
            # config.gpu_options.allow_growth = True
            # sess = tf.Session(config=config)
            # tf.compat.v1.keras.backend.set_session(tf.Session(graph=tf.compat.v1.get_default_graph(), config=config))

            model, t_mean, t_log_var = self.get_vae_model()

            # Load VAE that was jointly trained with a
            # predictor returned from create_predictor()
            print("load_weights cnn_classifier: ", self.transferweights_classifier, ' exists: ', os.path.isfile(self.transferweights_classifier))
            model.load_weights(self.transferweights_classifier)

            print("layers:")
            for layer in model.layers:
                print(layer.name)
            print("removing the top layers")

            try:
                base = model.get_layer('base')
                del model
                # K.clear_session()
                gc.collect()
                return base

            except Exception as e:
                print(e)
                encoder = model.get_layer('encoder')
                del model
                encoder.summary()
                print("removing the top layers")
                base = Sequential()
                to_remove = ['t_mean', 't_log_var', 'conv2d', 'conv2d_1', 'conv2d_2', 'conv2d_3']
                for layer in encoder.layers:  # go through until last layer
                    print("layer.name: ", layer.name)
                    if layer.name not in to_remove:
                        print(layer.output_shape)
                        base.add(layer)
                base.summary()

                del encoder
                # K.clear_session()
                gc.collect()

                return base


    def setup_model(self):
        if self.preprocessing_type_classifier == 'bvae':
            image_shape = (self.bottleneck)
        else:
            image_shape = (self.dim_resized_patch, self.dim_resized_patch, self.channels_number)
        if self.transferweights_classifier is None:
            if self.nb_of_class ==2:
                if self.type == 'vgg16':
                    self.model = create_binary_classifier_vgg16(image_shape, weights=self.pretrained_keras, b_untrainable=True,
                                                                dense_units=128)

                elif self.type == 'yolo2':
                    #image_shape, b_untrainable=True, dense_units=128, activation='relu', n=5
                    self.model = create_binary_classifier_yolo2(image_shape, dense_units=128)
                elif self.type == 'resnet50':
                    self.model = create_binary_resnet50(image_shape, weights=self.pretrained_keras, b_untrainable=True,
                                                        dense_units=128)
                elif self.type == 'small':
                    self.model = create_binary_classifier_small(image_shape,
                                                        dense_units=1024)

            else:
                if self.type == 'vgg16':
                    self.model = create_multilabel_classifier_vgg16(image_shape, self.nb_of_class, weights=self.pretrained_keras,
                                                                b_untrainable=True,
                                                                dense_units=128)
                elif self.type == 'yolo2':
                    # image_shape, b_untrainable=True, dense_units=128, activation='relu', n=5
                    self.model = create_multilabel_classifier_yolo2(image_shape, self.nb_of_class, dense_units=128)
                elif self.type == 'resnet50':
                    self.model = create_multilabel_resnet50(image_shape, self.nb_of_class, weights=self.pretrained_keras, b_untrainable=True,
                                                        dense_units=128)
                elif self.type == 'small':
                    self.model = create_multilabel_classifier_small(image_shape,self.nb_of_class,
                                                        dense_units=1024)
            if self.trained_model is not None:
                self.model.load_weights(self.trained_model)

        else:
            base = self.load_vae_model()
            if self.nb_of_class ==2:
                self.model = create_binary_classifier_with_base(base, b_untrainable=True, dense_units=128)
            else:
                self.model = create_multilabel_classifier_with_base(base, self.nb_of_class, b_untrainable=True, dense_units=128)

        # image_shape = (self.bottleneck)
        # self.model = create_binary_classifier_small(image_shape, dense_units=1024)
        if self.model is not None:
            self.model.summary()

    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu=None, dict_encoded_var=None, with_timestamp=True):
        if self.model is not None:
            del self.model
            K.clear_session()
            ops.reset_default_graph()
            gc.collect()

        self.setup_model()

        #model, X_train_grasps, X_val_grasps, dict_rgb_scene, dict_depth_scene, config, preprocessing_fct

        X_train_grasps = GT_grasps_training
        X_val_grasps = GT_grasps_validation


        network_name = get_name_network(self.type, self.use_rgb, self.use_gray_depth, self.use_SN, None, self.use_SN_filter,
                                        self.channels_number,
                                        None, self.dim_resized_patch, self.random_sizes)

        if self.train_batch_size > len(X_train_grasps):
            train_batch_size = len(X_train_grasps)

        if self.validation_batch_size > len(X_val_grasps):
            validation_batch_size = len(X_val_grasps)



        if self.allow_flip:
            random_angle_augmentation_degrees = [0.0, -180.0, 180.0]
        else:
            random_angle_augmentation_degrees = [0.0]

        optimizer = select_optimizer(optimizer=self.optimizer, base_learning_rate=self.base_learning_rate)
        loss_list = []
        if self.nb_of_class == 2:
            loss_list.append('binary_crossentropy')
        else:
            loss_list.append('categorical_crossentropy')

        loss_weights_list = []
        loss_weights_list.append(1.0)

        self.model.compile(optimizer=optimizer,
                           loss=loss_list,
                           loss_weights=loss_weights_list, metrics=['accuracy'])



        if self.nb_of_class ==2:
            current_label_dim = 11
        else:
            current_label_dim = 12

            for i in range(len(X_train_grasps)):
                # print("before X_train_grasps[i]: ", X_train_grasps[i])

                X_train_grasps[i] = list(X_train_grasps[i])
                X_train_grasps[i].append(dict_id_scene_id_obj[X_train_grasps[i][1]])
                # print("after X_train_grasps[i]: ", X_train_grasps[i])
                if X_train_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
                    self.dict_sqlid_categorical_id[X_train_grasps[i][current_label_dim]] = X_train_grasps[i][current_label_dim]

            for i in range(len(X_val_grasps)):
                X_val_grasps[i] = list(X_val_grasps[i])
                X_val_grasps[i].append(dict_id_scene_id_obj[X_val_grasps[i][1]])
                if X_val_grasps[i][current_label_dim] not in self.dict_sqlid_categorical_id:
                    self.dict_sqlid_categorical_id[X_val_grasps[i][current_label_dim]] = X_val_grasps[i][current_label_dim]

            # print("before self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)
            keys = [k for k in self.dict_sqlid_categorical_id.keys()]
            keys.sort()
            start = 0
            for k in keys:
                self.dict_sqlid_categorical_id[k]=start
                start+=1
            # print("after self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)

            for i in range(len(X_train_grasps)):
                X_train_grasps[i][current_label_dim] = self.dict_sqlid_categorical_id[X_train_grasps[i][current_label_dim]]

            for i in range(len(X_val_grasps)):
                X_val_grasps[i][current_label_dim] = self.dict_sqlid_categorical_id[X_val_grasps[i][current_label_dim]]




        if self.preprocessing_type_classifier == 'bvae':
            my_training_batch_generator = My_Custom_Generator_bvae(X_train_grasps, train_batch_size,
                                                    preprocessing_fct=self.preprocessing_function,
                                                    dic_images_rgb=dict_rgb_scene,
                                                    dic_depth_raw_data=dict_depth_scene,
                                                    b_with_augmentation=self.allow_flip,
                                                   dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var, b_classifier=True, latent_dim=self.bottleneck, label_dim=current_label_dim, num_classes=self.nb_of_class)



            my_validation_batch_generator = My_Custom_Generator_bvae(X_val_grasps, validation_batch_size,
                                                    preprocessing_fct=self.preprocessing_function,
                                                    dic_images_rgb=dict_rgb_scene,
                                                    dic_depth_raw_data=dict_depth_scene,
                                                    b_with_augmentation=False,
                                                     dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var, b_classifier=True, latent_dim=self.bottleneck, label_dim=current_label_dim, num_classes=self.nb_of_class)

            training_steps = int(math.ceil(my_training_batch_generator.n // train_batch_size))
            print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)

            validation_steps = int(math.ceil(my_validation_batch_generator.n // validation_batch_size))

        else:
            my_training_batch_generator = My_Custom_Generator(X_train_grasps, train_batch_size,
                                                              channels_number=self.channels_number,
                                                              allow_flip=self.allow_flip,
                                                              random_angles=random_angle_augmentation_degrees,
                                                              label_shape=(-1, len(X_train_grasps)),
                                                              final_size=self.dim_resized_patch, random_sizes=self.random_sizes,
                                                              dic_images_rgb=dict_rgb_scene,
                                                              dic_depth_raw_data=dict_depth_scene, use_rgb=self.use_rgb,
                                                              use_gray_depth=self.use_gray_depth, use_SN=self.use_SN,
                                                              b_filter=self.use_SN_filter, b_classifier=True,
                                                              preprocessing_fct=self.preprocessing_function, label_dim=current_label_dim, num_classes=self.nb_of_class)

            my_validation_batch_generator = My_Custom_Generator(X_val_grasps, validation_batch_size,
                                                                allow_flip=False,
                                                                # random_angles=random_angle_augmentation_degrees,
                                                                channels_number=self.channels_number,
                                                                label_shape=(-1, len(X_val_grasps)),
                                                                final_size=self.dim_resized_patch, random_sizes=self.random_sizes,
                                                                dic_images_rgb=dict_rgb_scene,
                                                                dic_depth_raw_data=dict_depth_scene, use_rgb=self.use_rgb,
                                                                use_gray_depth=self.use_gray_depth, use_SN=self.use_SN,
                                                                b_filter=self.use_SN_filter, b_classifier=True,
                                                                preprocessing_fct=self.preprocessing_function, label_dim=current_label_dim, num_classes=self.nb_of_class)

            training_steps = int(math.ceil(my_training_batch_generator.n // train_batch_size))
            print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)

            validation_steps = int(math.ceil(my_validation_batch_generator.n // validation_batch_size))

        if with_timestamp:
            timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
        else:
            timestamp = ''

        save_dir = where_to_save_model
        if save_dir is not None:
            dir_path = save_dir + "/" + timestamp + "/"


            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            copyfile(config_path, dir_path + "config.ini")

            print("saving: X_train_grasps")
            save_obj(X_train_grasps, dir_path + "X_train_grasps.pkl")

            print("saving: X_val_grasps")
            save_obj(X_val_grasps, dir_path + "X_val_grasps.pkl")

            save_file = os.path.join(dir_path, network_name)

            tb = TensorBoard(log_dir=dir_path + "/", write_graph=True, update_freq='epoch')

            checkpoint = ModelCheckpoint(filepath=save_file, monitor='val_loss',
                                     verbose=0, save_best_only=True, save_weights_only=True, mode='min', )
            lr_monitor = LRTensorBoard(log_dir=dir_path + "/")

        else:
            tb = None
            checkpoint = None
            lr_monitor = None
            dir_path = None

        lr_schedule = ReduceLROnPlateau(monitor='val_loss', factor=self.decay_factor,
                                        mode='min', patience=self.scheduler_patience, min_lr=self.min_lr, min_delta=0.01)

        # lr_monitor = LRTensorBoard(log_dir=dir_path + "/")

        # lr_early_stopping = EarlyStoppingByLrVal(value=1e-06)
        lr_early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', patience=self.max_epoch_without_improvement,
                                                             restore_best_weights=True, min_delta=0.01)

        l_callbacks = []
        for c in [tb, checkpoint, lr_schedule, lr_early_stopping, lr_monitor]:
            if c is not None:
                l_callbacks.append(c)

        # for l in model.layers:
        #     print(l.name, l.trainable)

        print("len X_train_grasps: ", len(X_train_grasps))
        print("len X_val_grasps: ", len(X_val_grasps))
        print("train_batch_size: ", train_batch_size)
        print("train_batch_size: ", validation_batch_size)
        print("my_training_batch_generator.n: ", my_training_batch_generator.n)

        start_time = time.time()
        # self.model.fit_generator(generator=my_training_batch_generator,
        #                          epochs=max_epochs,
        #                          steps_per_epoch=training_steps,
        #                          shuffle=True,
        #                          validation_data=my_validation_batch_generator,
        #                          validation_steps=validation_steps,
        #                          callbacks=l_callbacks,
        #                          # callbacks=[lr_schedule, lr_early_stopping],
        #                          verbose=1)

        self.model.fit(x=my_training_batch_generator,
                            epochs=self.max_epochs,
                            steps_per_epoch=training_steps,
                            shuffle=True,
                            validation_data=my_validation_batch_generator,
                            validation_steps=validation_steps,
                            callbacks=[lr_schedule, lr_early_stopping],
                            verbose=1)

        elapsed_time = time.time() - start_time
        if dir_path is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            # self.save_weights(save_file)


    def load_weights(self,path_to_weights):
        print("load_weights CNNClassifier")
        if self.b_force_cpu:
            device="cpu:0"
        else:
            device="gpu:0"
        with tf.device(device):
            config = tf.ConfigProto(allow_soft_placement=True)
            config.gpu_options.per_process_gpu_memory_fraction = 0.3
            config.gpu_options.allow_growth = True
            sess = tf.Session(config=config)
            print("load_weights cnn_classifier: ", path_to_weights, ' exists: ', os.path.isfile(path_to_weights))

            self.model.load_weights(path_to_weights)

    def del_model(self):
        print("del bvae network")

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        gc.collect

        del self.model


        tf.reset_default_graph()
        # K._SESSION.close()
        # K._SESSION = None

        K.clear_session()
        # device = cuda.get_current_device()
        # device.reset()
        # cuda.select_device(0)
        # cuda.close()
        gc.collect()

        self.model = None

    # def save_weights(self, path_to_weights):
    #     dump(self.model, path_to_weights)

    def save_weights(self, path_to_weights):
        self.model.save(path_to_weights+ self.type)

    # def fit_with_validation(self,X_train_grasps, X_val_grasps, dict_rgb_scene, dict_depth_scene, preprocessing_fct, where_to_save_model):
    #
    #     dim_resized_patch = self.config.getint('bvae', 'dim_resized_patch')
    #     channels_number = self.config.getint('bvae', 'channels_number')
    #
    #     random_sizes_str = self.config.get('bvae_training', 'random_sizes')
    #     random_sizes = random_sizes_str.split('\n')
    #     random_sizes = [int(s) for s in random_sizes]
    #
    #     optimizer = self.config.get('bvae_training', 'optimizer')
    #     base_learning_rate = self.config.getfloat('bvae_training', 'base_learning_rate')
    #     train_batch_size = self.config.getint('classifier', 'max_batch_size')
    #     validation_batch_size = self.config.getint('classifier', 'max_batch_size')
    #
    #     if train_batch_size > len(X_train_grasps):
    #         train_batch_size = len(X_train_grasps)
    #
    #     if validation_batch_size > len(X_val_grasps):
    #         validation_batch_size = len(X_val_grasps)
    #
    #     decay_factor = self.config.getfloat('bvae_training', 'decay_factor')
    #     max_epochs = self.config.getint('bvae_training', 'max_epochs')
    #     use_rgb = self.config.getboolean('bvae', 'use_rgb')
    #     use_gray_depth = self.config.getboolean('bvae', 'use_gray_depth')
    #     use_SN = self.config.getboolean('bvae', 'use_SN')
    #     use_SN_filter = self.config.getboolean('bvae', 'use_SN_filter')
    #
    #     network_name = get_name_network(self.type, use_rgb, use_gray_depth, use_SN, None, use_SN_filter, channels_number,
    #                                     None, dim_resized_patch, random_sizes)
    #
    #     allow_flip = self.config.getboolean('classifier', 'use_augmentation')
    #     if allow_flip:
    #         random_angle_augmentation_degrees = [0.0, -180.0, 180.0]
    #     else:
    #         random_angle_augmentation_degrees = [0.0]
    #
    #     optimizer = select_optimizer(optimizer=optimizer, base_learning_rate=base_learning_rate)
    #     loss_list = []
    #     loss_list.append('binary_crossentropy')
    #     loss_weights_list = []
    #     loss_weights_list.append(1.0)
    #
    #     self.model.compile(optimizer=optimizer,
    #                   loss=loss_list,
    #                   loss_weights=loss_weights_list, metrics=['accuracy'])
    #
    #     print("len X_train_grasps: ", len(X_train_grasps))
    #
    #     my_training_batch_generator = My_Custom_Generator(X_train_grasps, train_batch_size,
    #                                                       channels_number=channels_number,
    #                                                       allow_flip=allow_flip,
    #                                                       random_angles=random_angle_augmentation_degrees,
    #                                                       label_shape=(-1, len(X_train_grasps)),
    #                                                       final_size=dim_resized_patch, random_sizes=random_sizes,
    #                                                       dic_images_rgb=dict_rgb_scene,
    #                                                       dic_depth_raw_data=dict_depth_scene, use_rgb=use_rgb,
    #                                                       use_gray_depth=use_gray_depth, use_SN=use_SN,
    #                                                       b_filter=use_SN_filter, b_classifier=True,
    #                                                       preprocessing_fct=preprocessing_fct)
    #
    #     print("len X_val_grasps: ", len(X_val_grasps))
    #     my_validation_batch_generator = My_Custom_Generator(X_val_grasps, validation_batch_size,
    #                                                         allow_flip=allow_flip,
    #                                                         random_angles=random_angle_augmentation_degrees,
    #                                                         channels_number=channels_number,
    #                                                         label_shape=(-1, len(X_train_grasps)),
    #                                                         final_size=dim_resized_patch, random_sizes=random_sizes,
    #                                                         dic_images_rgb=dict_rgb_scene,
    #                                                         dic_depth_raw_data=dict_depth_scene, use_rgb=use_rgb,
    #                                                         use_gray_depth=use_gray_depth, use_SN=use_SN,
    #                                                         b_filter=use_SN_filter, b_classifier=True,
    #                                                         preprocessing_fct=preprocessing_fct)
    #
    #
    #     timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
    #
    #     save_dir = where_to_save_model
    #     dir_path = save_dir + "/" + timestamp + "/"
    #
    #     tb = TensorBoard(log_dir=dir_path + "/", write_graph=True, update_freq='epoch')
    #
    #     if not os.path.exists(save_dir):
    #         os.mkdir(save_dir)
    #
    #     if not os.path.exists(dir_path):
    #         os.mkdir(dir_path)
    #
    #     copyfile(config_path, dir_path + "config.ini")
    #
    #     print("saving: X_train_grasps")
    #     save_obj(X_train_grasps, dir_path + "X_train_grasps.pkl")
    #
    #     print("saving: X_val_grasps")
    #     save_obj(X_val_grasps, dir_path + "X_val_grasps.pkl")
    #
    #
    #     save_file = os.path.join(dir_path, network_name)
    #
    #     checkpoint = ModelCheckpoint(filepath=save_file, monitor='val_loss',
    #                                  verbose=0, save_best_only=True, save_weights_only=True, mode='auto')
    #
    #     lr_schedule = ReduceLROnPlateau(monitor='val_loss', factor=decay_factor,
    #                                     mode='min', patience=50, min_lr=1e-06)
    #
    #     # lr_early_stopping = EarlyStoppingByLrVal(value=1e-06)
    #     lr_early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', patience=50,
    #                                                          restore_best_weights=True)
    #
    #     # for l in model.layers:
    #     #     print(l.name, l.trainable)
    #
    #     print("train_batch_size: ", train_batch_size)
    #     print("my_training_batch_generator.n: ", my_training_batch_generator.n)
    #     training_steps = int(math.ceil(my_training_batch_generator.n // train_batch_size))
    #     print("training_steps - my_training_batch_generator.n // train_batch_size: ", training_steps)
    #
    #     validation_steps = int(math.ceil(my_validation_batch_generator.n // validation_batch_size))
    #
    #     self.model.fit_generator(generator=my_training_batch_generator,
    #                         epochs=max_epochs,
    #                         steps_per_epoch=training_steps,
    #                         shuffle=True,
    #                         validation_data=my_validation_batch_generator,
    #                         validation_steps=validation_steps,
    #                         # callbacks=[tb, checkpoint, lr_schedule, lr_monitor, lr_early_stopping],
    #                         callbacks=[lr_schedule, lr_early_stopping],
    #                         verbose=1)


    # def predict(self,X):
    #
    #     # X = self.scaler.transform(X)
    #     # proba = self.model.predict_proba(X)[:,1]
    #     proba = self.model.predict(X)
    #     # r = np.asarray([proba[i][0] for i in range(len(proba))]).reshape((-1,))
    #     if self.nb_of_class == 2:
    #         r = list([float(proba[i][0]) for i in range(len(proba))])
    #     # print("r: ", r)
    #     # for i in range(len(proba)):
    #     #     print("Predicted: ", proba[i])
    #     return r, None


    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        if self.preprocessing_type_classifier == 'bvae':
            X  = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)

        else:
            X = []
            for g in grasps_candidates:
                # X.append(extract_single_feature(dict_rgb_scene[g[1]], dict_depth_raw[g[1]], g[3], g[4],
                #                        math.degrees(g[8]), self.use_rgb, self.use_gray_depth, self.use_SN,
                #                        self.use_SN_filter, self.dim_resized_patch, self.dim_resized_patch, flipped=0))
                # X.append(extract_single_feature(dict_rgb_scene[g[1]], dict_depth_raw[g[1]], g, self.use_rgb, self.use_gray_depth, self.use_SN,
                #                        self.use_SN_filter, self.dim_resized_patch, self.dim_resized_patch, flipped=0))
                X.append(extract_single_feature(rgb_scene=dict_rgb_scene[g[1]], depth_raw=dict_depth_raw[g[1]], grasp_position_x=g[3], grasp_position_y=g[4], angle_degrees=math.degrees(g[8]),
                                                use_rgb=self.use_rgb, use_gray_depth=self.use_gray_depth, use_SN=self.use_SN, b_filter=self.use_SN_filter,
                                                dim_grasp_patch=self.dim_resized_patch, dim_resized_patch=self.dim_resized_patch, flipped=0))

            X = np.array(X)
            X  = self.preprocessing_function(X)


        start_time = time.time()
        proba = self.model.predict(X)
        elapsed_time = time.time() - start_time

        if self.nb_of_class == 2:
            r = list([float(proba[i][0]) for i in range(len(proba))])
        else:
            r = proba
        # print("r: ", r)
        # for i in range(len(proba)):
        #     print("Predicted: ", proba[i])
        return r, None, None, elapsed_time
