from .sklearn_classifier_template import SklearnClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
import torch
import time
import numpy as np

class SVMClassification(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        C = 1.0  # SVM regularization parameter
        # self.model = LinearSVC(C=C, max_iter=10000) #40.23668639053255
        #{'linear', 'poly', 'rbf', 'sigmoid', 'precomputed'}
        # self.model = LinearSVC(C=C, max_iter=-1) #30.76923076923077
        # self.model = SVC(kernel="linear", C=C) #39.053254437869825
        # self.model = SVC(kernel="sigmoid", gamma=0.7, C=C) #40.23668639053255
        # self.model = SVC(kernel="poly", degree=2, gamma="auto", C=C) #42.60355029585799
        # self.model = SVC(kernel="poly", degree=3, gamma="auto", C=C) #46.15384615384615
        # self.model = SVC(kernel="poly", degree=4, gamma="auto", C=C) #44.37869822485207
        # self.model = SVC(kernel="rbf", gamma=1.0, C=C) #50.887573964497044
        # self.model = SVC(kernel="rbf", gamma=0.9, C=C) #53.84615384615385
        # self.model = SVC(kernel="rbf", gamma=0.6, C=C) #52.662721893491124
        # self.model = SVC(kernel="rbf", gamma=0.7, C=C) #55.02958579881657 + standard 68.04733727810651 + robust 69.23076923076923
        # self.model = SVC(kernel="rbf", gamma=0.8, C=C) #53.25443786982249
        # self.model = SVC(kernel="rbf", gamma=0.7, C=C, shrinking=False) #robust 70.4
        self.model = SVC(kernel="rbf", gamma=0.7, C=C, shrinking=False, tol=1e-3) #robust 70.4
        # self.model = NuSVC(kernel="rbf", nu=0.7, shrinking=False, tol=1e-3) #robust 70.4


    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        if self.by_object:
            print('predict by_object')
            r_full = []
            X_mu_full = None
            X_var_full = None
            elapsed_time_full = 0.0
            self.by_object = False
            #grasp candidates are already sorted
            # id_obj is in 12

            index_last=0
            while index_last != len(grasps_candidates)-1:
                index_start = index_last
                for i in range(index_start,len(grasps_candidates)-1):
                    if grasps_candidates[i][12] != grasps_candidates[i+1][12]:
                        break
                    index_last=i+1

                print("index_start: ", index_start)
                print("index_last: ", index_last)

                #only one label
                id_obj = grasps_candidates[index_start][12]
                if id_obj in self.dict_obj_grasp_training:
                    GT_grasps_training = self.dict_obj_grasp_training[id_obj]
                else:
                    GT_grasps_training = []

                if id_obj in self.dict_obj_grasp_validation:
                    GT_grasps_validation = self.dict_obj_grasp_validation[id_obj]
                else:
                    GT_grasps_validation = []

                print('GT_grasps_training: ', GT_grasps_training)
                print('GT_grasps_validation: ', GT_grasps_validation)
                sum_pos = 0
                sum_neg = 0
                for g in GT_grasps_training:
                    if g[11] > 0:
                        sum_pos+=1
                    else:
                        sum_neg+=1

                for g in GT_grasps_validation:
                    if g[11] > 0:
                        sum_pos += 1
                    else:
                        sum_neg += 1
                #todo maybe check taht we have at least one pos and one neg example
                if len(GT_grasps_training) + len(GT_grasps_validation) >= 2 and sum_pos > 0 and sum_neg > 0:
                    if self.latest_id_obj_trained != id_obj:
                        print('fitting model ', id_obj)
                        self.latest_id_obj_trained = id_obj
                        self.scaler = None
                        self.fit(self.dict_rgb_scene, self.dict_depth_scene, self.dict_background_scene, self.dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model=None, config_path=None, verbose=False, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp =False)
                else:
                    self.model = self.default_model
                    self.scaler = self.default_scaler

                current_grasp_candidates = grasps_candidates[index_start:index_last]
                print('len(current_grasp_candidates): ', len(current_grasp_candidates))
                X_mu, X_var = self.preprocessing_function(current_grasp_candidates, dict_rgb_scene, dict_depth_raw)

                if self.scaler is not None:
                    X_mu = self.scaler.transform(X_mu)

                try:
                    X_mu = torch.from_numpy(X_mu).to(self.device).float()
                except:
                    X_mu = torch.from_numpy(np.array(X_mu)).to(self.device).float()

                start_time = time.time()
                # r = self.model.predict(X_mu)
                r = list(self.model.decision_function(X_mu))

                elapsed_time = time.time() - start_time

                r_full+=r
                if X_mu_full is None:
                    X_mu_full = X_mu
                else:
                    X_mu_full+=X_mu

                if X_var_full is None:
                    X_var_full = X_var
                else:
                    X_var_full += X_var

                elapsed_time_full+=elapsed_time


            self.by_object = True
            return r_full, X_mu_full, X_var_full, elapsed_time

        else:
            X_mu, X_var = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)

            if self.scaler is not None:
                X_mu = self.scaler.transform(X_mu)

            try:
                X_mu = torch.from_numpy(X_mu).to(self.device).float()
            except:
                X_mu = torch.from_numpy(np.array(X_mu)).to(self.device).float()


            start_time = time.time()
            # r = self.model.predict(X_mu)
            r = self.model.decision_function(X_mu)

            elapsed_time = time.time() - start_time
            # r = list([float(proba[i][1]) for i in range(len(proba))])
            try:
                return r, X_mu.cpu().data.numpy(), X_var.cpu().data.numpy(), elapsed_time
            except:
                return r, X_mu, X_var, elapsed_time
