from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import Matern
from .sklearn_classifier_template import SklearnClassifier

class SklearnGPClassifier(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        self.model = GaussianProcessClassifier(kernel= 1**2 * Matern(length_scale=1, nu=1.5), random_state=0, n_restarts_optimizer=1)
