from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from .sklearn_regression_template import SklearnRegressor
import time
import torch

class GPRegression(SklearnRegressor):
    #joblib
    def __init__(self, config, preprocessing_function, prior_length_function=None):
        super().__init__(config, preprocessing_function, prior_length_function)

    def setup_model(self):
        self.model_length = GaussianProcessRegressor(kernel=1.0 * RBF(length_scale=1.0), n_restarts_optimizer=1, alpha=0.1)

    def predict_preprocessed_length(self,X):
        # X = self.preprocessing_function(X)
        return self.model_length.predict(X, return_std=False)

    def predict_length(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        start_time = time.time()
        X_mu, X_var = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)

        # X = self.pca.transform(X)
        # X = np.concatenate((X, X_var), axis=1).astype(np.float32)
        # X = (X - self.min) / (self.max - self.min)
        X_mu = torch.from_numpy(X_mu).to('cpu').float()

        if self.scaler is not None:
            X = self.scaler.transform(X_mu)
            means = self.model_length.predict(X, return_std=False)
        else:
            means = self.model_length.predict(X_mu, return_std=False)

        elapsed_time = time.time() - start_time

        return means, X_mu, X_var, elapsed_time