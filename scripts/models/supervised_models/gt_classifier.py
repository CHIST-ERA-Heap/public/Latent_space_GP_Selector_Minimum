import time

class GroundTruthClassifier:
    def __init__(self, config, preprocessing_function, base = None):
        self.config = config
        self.base = base
        self.nb_of_class = 2
        self.preprocessing_classifier = preprocessing_function
        self.preprocessing_type_classifier = self.config.get('feature_extraction', 'preprocessing')

        # args = parse_arguments(sys.argv[1:])[0]
        self.use_SN = self.config.getboolean('data_modalities', 'use_SN')
        self.use_gray_depth = self.config.getboolean('data_modalities', 'use_gray_depth')
        self.use_rgb = self.config.getboolean('data_modalities', 'use_rgb')

        self.max_depth_value = self.config.getfloat('data_modalities', 'max_depth_value')
        self.channels_number = self.config.getint('data_modalities', 'channels_number')
        self.dim_resized_patch = self.config.getint('data_modalities', 'dim_resized_patch')
        self.dim_grasp_patch = self.config.getint('data_modalities', 'dim_grasp_patch')
        self.projection_dim = self.config.getint('feature_extraction', 'projection_dim')
        self.max_input_size = self.config.getint('feature_extraction', 'max_input_size')
        self.train_batch_size = self.config.getint('classifier', 'max_batch_size')
        self.validation_batch_size = self.config.getint('classifier', 'max_batch_size')
        self.allow_flip = self.config.getboolean('classifier', 'use_augmentation')
        self.base_learning_rate = self.config.getfloat('classifier', 'lr')
        self.min_lr = self.config.getfloat('classifier','min_lr')
        self.random_sizes = [self.dim_grasp_patch]
        self.max_epoch_without_improvement = self.config.getint('classifier', 'max_epoch_without_improvement')
        self.scheduler_patience = self.config.getint('classifier', 'scheduler_patience')

        self.b_force_cpu = not self.config.getboolean('feature_extraction', 'use_gpu')

        self.decay_factor = self.config.getfloat('transfer_learning', 'decay_factor')
        self.from_where = self.config.get('transfer_learning', 'from_where')
        self.optimizer = self.config.get('transfer_learning', 'optimizer')
        self.max_epochs = self.config.getint('classifier', 'max_number_of_epochs')
        self.pretrained_model = self.config.get('transfer_learning', 'pretrained_model')
        self.fine_tuning = self.config.getboolean('transfer_learning', 'fine_tuning')

        self.train_with_classifier_data = self.config.getboolean('grasp_object_classifier','train_with_classifier_data')
        self.model_type = self.config.get('feature_extraction', 'model_type')
        self.trained_model = self.config.get('classifier', 'trained_model')
        self.by_object = self.config.getboolean('classifier','by_object')
        self.dict_object_id_categorical_id = {}
        self.dict_id_scene_id_obj = {}
        self.new = 0

        if self.trained_model == "None":
            self.trained_model = None

        if self.pretrained_model == "None":
            self.pretrained_model = None

        self.model = None

    def setup_model(self):
        pass

    def del_network(self):
        pass

    def load_model(self, path_to_weights):
        pass

    def predict(self,grasps_candidates, dict_rgb_scene, dict_depth_raw, reverse=True, verbose=False):
        start_time = time.time()
        if verbose:
            print('self.dict_object_id_categorical_id: ', self.dict_object_id_categorical_id)
        for i in range(len(grasps_candidates)):
            if verbose:
                print('before grasps_candidates[i]: ', grasps_candidates[i])

            id_scene = grasps_candidates[i][1]

            if id_scene in self.dict_id_scene_id_obj:
                id_obj = self.dict_id_scene_id_obj[id_scene]
                id_categorical = self.dict_object_id_categorical_id[id_obj]
            else:
                id_obj = 1

            #grasps_candidates[i][1] should be predicted
            grasps_candidates[i].append(id_obj)
            if verbose:
                print('id_scene: ', id_scene)
                print('id_obj: ', id_obj)
                print('id_categorical: ', id_categorical)
                print('after grasps_candidates[i]: ', grasps_candidates[i])

        #sort
        grasps_candidates.sort(key=lambda x: x[12], reverse=reverse)
        elapsed_time = time.time() - start_time

        return grasps_candidates, None, None, elapsed_time

    #fill a dict with object_id to categorical id
    def fill_dict_object_id_categorical_id(self, X_train_grasps, X_val_grasps, dict_id_scene_id_obj, current_label_dim = 12, verbose=False):

        if verbose:
            print('fill_dict_sqlid_categorical_id: ')

        for i in range(len(X_train_grasps)):
            # print("before X_train_grasps[i]: ", X_train_grasps[i])

            X_train_grasps[i] = list(X_train_grasps[i])
            if len(X_train_grasps[i]) != 13:
                X_train_grasps[i].append(dict_id_scene_id_obj[X_train_grasps[i][1]])
            if verbose:
                print("after X_train_grasps[i]: ", X_train_grasps[i])
            if X_train_grasps[i][current_label_dim] not in self.dict_object_id_categorical_id:
                self.dict_object_id_categorical_id[X_train_grasps[i][current_label_dim]] = X_train_grasps[i][
                    current_label_dim]

        for i in range(len(X_val_grasps)):
            X_val_grasps[i] = list(X_val_grasps[i])
            X_val_grasps[i].append(dict_id_scene_id_obj[X_val_grasps[i][1]])
            if verbose:
                print("after X_val_grasps[i]: ", X_val_grasps[i])
            if X_val_grasps[i][current_label_dim] not in self.dict_object_id_categorical_id:
                self.dict_object_id_categorical_id[X_val_grasps[i][current_label_dim]] = X_val_grasps[i][current_label_dim]

        # print("before self.dict_object_id_categorical_id: ", self.dict_object_id_categorical_id)
        keys = [k for k in self.dict_object_id_categorical_id.keys()]
        keys.sort()
        start = 0
        for k in keys:
            if verbose:
                print('k: ', k, ' start: ', start)
            self.dict_object_id_categorical_id[k] = start
            start += 1
        return X_train_grasps, X_val_grasps
        # print("after self.dict_sqlid_categorical_id: ", self.dict_sqlid_categorical_id)


    def fit(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu=None, dict_encoded_var=None, with_timestamp=True):
        print('fitting gt_classifier')
        current_label_dim = 12
        #dict_id_scene_id_obj
        self.dict_id_scene_id_obj = dict_id_scene_id_obj
        X_train_grasps, X_val_grasps = self.fill_dict_object_id_categorical_id(GT_grasps_training, GT_grasps_validation,
                                                                           dict_id_scene_id_obj, current_label_dim)
        print('gt_classifier fitted')
        self.model = True
