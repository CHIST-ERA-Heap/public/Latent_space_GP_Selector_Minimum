from sklearn.svm import SVR
from .sklearn_regression_template import SklearnRegressor

class SVRegressor(SklearnRegressor):
    #joblib
    def __init__(self, config, preprocessing_function, prior_length_function=None):
        super().__init__(config, preprocessing_function, prior_length_function)

    def setup_model(self):
        # self.model_length = SVR(C=1.0, epsilon=0.2)
        self.model_length = SVR(C=1.0, epsilon=0.2)
