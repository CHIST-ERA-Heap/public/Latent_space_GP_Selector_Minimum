from .sklearn_classifier_template import SklearnClassifier
from sklearn.linear_model import LogisticRegression

class LogisticRegressionClassification(SklearnClassifier):
    def __init__(self, config, preprocessing_function):
        super().__init__(config, preprocessing_function)

    def setup_model(self):
        self.model = LogisticRegression(random_state=0, max_iter=100000)