import os
from scripts.models.common.data_generator import My_Custom_Generator_gpy
import torch
from shutil import copyfile
import time
from scripts.common.pickle_util import save_obj
import numpy as np
import joblib
from .scaler_utils import select_scaler


class SklearnRegressor:
    #joblib
    def __init__(self, config, preprocessing_function, prior_length_function=None):
        self.config = config
        self.preprocessing_function = preprocessing_function
        self.prior_length_function = prior_length_function

        self.model_length = None
        self.trained_model_length = self.config.get('length_regressor', 'trained_model')
        if self.trained_model_length == "None":
            self.trained_model_length = None
        # else:
        #     self.model_length = load(self.trained_model_length)
        self.b_with_augmentation = self.config.getboolean('length_regressor','use_augmentation')
        self.scaler_type = self.config.get('length_regressor', 'scaler_type')
        if self.scaler_type != 'None':
            self.scaler = select_scaler(self.scaler_type)
        else:
            self.scaler = None

        self.b_predict_correction = self.config.getboolean('length_regressor','predict_correction')
        self.b_force_fix_prior  = self.config.getboolean('length_regressor','force_fix_prior')
        self.prior_length  = self.config.getfloat('length_regressor','prior_length')

        self.device = torch.device('cpu')

    def setup_model(self):
        pass
        # self.model_length = GaussianProcessRegressor(kernel=1.0 * RBF(length_scale=1.0), n_restarts_optimizer=1, alpha=0.1)

    def load_model(self,path_to_weights):
        print('load_weights SklearnRegressor: ', path_to_weights,' exists ', os.path.isfile(path_to_weights))

        self.model_length = joblib.load(path_to_weights)
        if self.scaler is not None:
            self.scaler = joblib.load(path_to_weights.replace('.joblib','_scaler.joblib'))

    def save_weights(self, path_to_weights):
        joblib.dump(self.model_length, path_to_weights+'model.joblib')
        if self.scaler is not None:
            joblib.dump(self.scaler, path_to_weights + 'model_scaler.joblib')

    def fit_length(self, dict_rgb_scene, dict_depth_scene, dict_background_scene, dict_id_scene_id_obj, GT_grasps_training, GT_grasps_validation, where_to_save_model, config_path, verbose=False, dict_encoded_mu={}, dict_encoded_var={}, with_timestamp=False):
        print('fit_length')
        GT_grasps = GT_grasps_training + GT_grasps_validation

        GT_grasps_positives_only = []
        for g in GT_grasps:
            if g[11] > 0:
                GT_grasps_positives_only.append(g)

        # print("SklearnRegressor length regressor GT_grasps_positives_only: ", GT_grasps_positives_only)
        size_training = len(GT_grasps_positives_only)
        batch_size = size_training

        #todo add option to guess length to learn correction
        train_dataset = My_Custom_Generator_gpy(GT_grasps_positives_only, batch_size,
                                                preprocessing_fct=self.preprocessing_function,
                                                dic_images_rgb=dict_rgb_scene,
                                                dic_depth_raw_data=dict_depth_scene,
                                                b_with_augmentation=self.b_with_augmentation,
                                                dict_encoded_mu=dict_encoded_mu, dict_encoded_var=dict_encoded_var, label_dim=9, b_correction=self.b_predict_correction, prior_length=self.prior_length, b_force_fix_prior = self.b_force_fix_prior, prior_length_function=self.prior_length_function)

        X_mu, X_var, labels = train_dataset.get_all()

        # train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        # X_mu, X_var, labels = next(iter(train_loader))
        # X_mu = torch.reshape(X_mu, (batch_size, X_mu.size()[-1])).cpu().data.numpy()
        # X_var = torch.reshape(X_var, (batch_size, X_var.size()[-1])).cpu().data.numpy()
        # labels = labels.cpu().data.numpy()


        save_dir = where_to_save_model
        if save_dir is not None:
            if with_timestamp:
                timestamp = time.strftime("%m-%d-%Y_%H%M%S", time.localtime())
            else:
                timestamp = ''

            dir_path = save_dir + "/" + timestamp + "/"

            if not os.path.exists(save_dir):
                os.mkdir(save_dir)

            if not os.path.exists(dir_path):
                os.mkdir(dir_path)

            np.save(dir_path + 'X_mu.npy', X_mu)
            np.save(dir_path + 'X_var.npy', X_var)
            np.save(dir_path + 'Y.npy', labels)

        self.setup_model()
        start_time = time.time()

        if self.scaler is not None:
            self.scaler = self.scaler.fit(X_mu)
            # self.scaler = preprocessing.RobustScaler().fit(X_mu)

            X_scaled = self.scaler.transform(X_mu)
            X_scaled = torch.from_numpy(X_scaled).to(self.device).float()
            self.model_length.fit(X_scaled, labels)
        else:
            self.model_length.fit(X_mu, labels)

        elapsed_time = time.time() - start_time

        if save_dir is not None:
            elapsed_time_file = os.path.join(dir_path, "elapsed_time_file.pkl")
            save_obj(elapsed_time, elapsed_time_file)

            copyfile(config_path, dir_path + "config.ini")

            self.save_weights(dir_path)


    def fit_preprocessed_length(self,X,Y):
        self.setup_model()
        self.model_length.fit(X, Y)

    def predict_preprocessed_length(self,X):
        # X = self.preprocessing_function(X)
        return self.model_length.predict(X)

    def predict_length(self,grasps_candidates, dict_rgb_scene, dict_depth_raw):
        start_time = time.time()
        X_mu, X_var = self.preprocessing_function(grasps_candidates, dict_rgb_scene, dict_depth_raw)

        # X = self.pca.transform(X)
        # X = np.concatenate((X, X_var), axis=1).astype(np.float32)
        # X = (X - self.min) / (self.max - self.min)
        X_mu = torch.from_numpy(X_mu).to('cpu').float()

        if self.scaler is not None:
            X = self.scaler.transform(X_mu)
            means = self.model_length.predict(X)
        else:
            means = self.model_length.predict(X_mu)

        elapsed_time = time.time() - start_time

        return means, X_mu, X_var, elapsed_time
